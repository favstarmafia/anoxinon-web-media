+++
title = "Kontakt"
id = "contact"
+++

### Fragen, Anregungen, Mitmachen?

Wir freuen uns über jegliche Kontaktaufnahme, seien es Fragen, Verbesserungsvorschläge oder allgemeine Anfragen.
Solltest Du Interesse haben mitzuwirken, schaue hier vorbei: [Mitmachen](https://anoxinon.de/mitmachen/)  
Unsere Kommunikationskanäle haben wir weiter unten aufgelistet.

**Kontakt via E-Mail:**

Redaktion - Leitung: <redaktion@anoxinon.de>  
GPG Key: [0x4038E4839A17D48D](/keys/0x4038E4839A17D48D.txt) |
ID: E790 4472 54CE 0357 2C7B 2CB7 4038 E483 9A17 D48D  

**Kontakt via Fediverse:**

Mastodon: <a href="https://social.anoxinon.de/@anoxinonmedia">@AnoxinonMedia@social.anoxinon.de</a>

**Kontakt via XMPP:**

XMPP MUC: <a href="xmpp://anoxinon@conference.anoxinon.me">anoxinon@conference.anoxinon.me</a>

#### Achtung: Anfragen zu dem Verein Anoxinon e.V. oder dessen anderen Dienste sind bitte [direkt](https://anoxinon.de/kontakt/) an diesen zu richten.
