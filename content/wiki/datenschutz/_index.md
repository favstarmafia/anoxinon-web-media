---
title: Datenschutz
layout: wikipage
---  

Datenschutz ist ein Kernthema bei uns im Anoxinon e.V. und vieles, was wir im Verein betreiben, anbieten und erstellen beschäftigt sich mit dieser Thematik. Deshalb darf auch hier im Wiki keine Kapitel für diesen Aspekt der digitalen Welt fehlen.

## Einstieg

[**Flyer - Datenschutz für Klein & Groß**](/files/Flyer_Datenschutz_final_Webseite.pdf): Kurz und knapp zusammengefasst bietet dieser Flyer einen Überblick über die Rolle von Daten und Datenschutz im modernen Internet.

[**Deceived by Design**](https://fil.forbrukerradet.no/wp-content/uploads/2018/06/2018-06-27-deceived-by-design-final.pdf): Studie darüber, wie sich die großen Hersteller immer perfidere Methoden überlegen, dass Menschen sich gegen Datenschutz entscheiden in dem sie datenschutzunfreundlichere Maßnahmen einfacher verfügbar machen.

[**Google Data Collection Paper**](https://digitalcontentnext.org/wp-content/uploads/2018/08/DCN-Google-Data-Collection-Paper.pdf): Studie darüber, in welchem Umfang Google Daten sammelt .

[**Kids - Digital genial - Digitalcourage**](https://digitalcourage.de/sites/default/files/2018-06/Auszug_KidsDigitalGenial_Leseprobe.pdf): Lexikon der Digitalcourage (Leseprobe). Geht auf viele Dinge in der digitalen Welt ein, die auch über den Datenschutz hinaus gehen.

[**EDRI Digital Defenders**](https://edri.org/files/privacy4kids_booklet_web.pdf): Sehr gute Informationsbroschüre für Kids (erhältlich in Englisch, Deutsch und Französisch), die auch auf viele wichtige Dinge im Umgang mit dem Internet eingeht, die über den Datenschutz hinaus gehen. 
