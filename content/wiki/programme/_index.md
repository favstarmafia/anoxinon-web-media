---
title: Programme
layout: wikipage
---

In dieser Kategorie findet Ihr empfehlenswerte Programme für den PC und Mobilgeräte. Der Fokus liegt auf [freier Software](/blog/istfreiesoftwareserioes/) und einem respektvollen Umgang mit anfallenden Nutzungsdaten.
