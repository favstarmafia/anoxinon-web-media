---
title: Anwendungsentwicklung
layout: wikipage
--- 

## Entwicklungsumgebungen (IDEs)

### Cross-language

- [Eclipse (Windows, MacOS, Linux)](https://eclipse.org)
- [Atom (Windows, MacOS, Linux)](https://github.com/atom)
- [Visual Studio Code (Windows, MacOS, Linux)](https://code.visualstudio.com/)
- [Emacs (Linux)](https://gnu.org/s/emacs)  

### Java

- [IntelliJ IDEA (Windows, MacOS, Linux)](https://www.jetbrains.com/idea/)

### Python

- [Pycharm (Windows, MacOS, Linux)](https://jetbrains.com/pycharm)

## Markdown Editoren

- [TYPORA (Windows, Linux)](https://typora.io)  
