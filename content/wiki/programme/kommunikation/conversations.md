---
title: Conversations
layout: wikipage
--- 


Conversations ist eine quelloffener und datenschutzfreundlicher Messenger auf Basis von XMPP für Android.

## Download der App und Projektinfos

- [Herunterladen bei F-Droid](https://f-droid.org/packages/eu.siacs.conversations/)
- [Kaufen bei Google Play](https://play.google.com/store/apps/details?id=eu.siacs.conversations)
- [Kaufen bei Amazon](https://www.amazon.com/dp/B00WD35AAC/)


## Schnellstart mit Conversations

**Flyer - Conversations Anleitung:** Für einen [schnellen Einstieg in Conversations bieten wir einen Flyer zum Herunterladen an](/files/Anleitung_Conversations.pdf). 

## Weblinks

- [Projekt-Webseite](https://conversations.im)
- [Quelltext](https://github.com/iNPUTmice/Conversations)
