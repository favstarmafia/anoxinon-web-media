---
title: Sicherheit
layout: wikipage
--- 

## VPN

- [WireGuard (Android)](https://f-droid.org/en/packages/com.wireguard.android/)
- [OpenVPN (Android)](https://f-droid.org/packages/de.blinkt.openvpn/) 

## Firewall

- [AFWall+ (Android)](https://kuketz-blog.de/f-droid-und-afwall-android-ohne-google-teil4/)
- [NetGuard (Android)](https://f-droid.org/en/packages/eu.faircode.netguard/)  

## Trackingschutz

- [Blokada (Android)](https://f-droid.org/en/packages/org.blokada.alarm/)  
