---
title: Videos bzw. YouTube-Alternativen
layout: wikipage
---

Um Videos im Internet anzuschauen, hat sich YouTube als go-to Plattform etabliert. Das nicht ohne Grund - dort finden sich mittlerweile unzählige Stunden Videomaterial. Doch es gibt auch Alternativen zu Googles Videoportal.

## Datenschutzfreundliche YouTube-Player

Wenn man nicht auf YouTube verzichten kann, dann kann man zumindest einen Player verwenden, der nur selbst nicht trackt und nur so wenig Daten wie möglich an Google übermittelt. Einige Beispiele listen wir nachfolgend.

Neben unserer Liste findet man auch weitere Aufzählungen von Invidious-Instanzen:

- [Invidious Instanzen](https://invidio.us) - Spiegelung von YouTube-Videos, YouTube erhält nur noch die IP-Adresse

Eine Auswahl:

- [SP-Codes Invidious](https://invidious.sp-codes.de/) - Invidious-Instanz von SP-Codes ([Datenschutzerklärung](https://sp-codes.de/de/privacy), [Verantwortliche:r](https://sp-codes.de/de/imprint/))
- [Snopyta Invidious](https://invidious.snopyta.org/) - Invidious-Instanz von Snopyta ([Datenschutzerklärung](https://snopyta.org/privacy_policy/), [Verantwortliche:r](https://snopyta.org/legal/))

## PeerTube - föderierte Videoplattform

Informationen zu PeerTube:

- [Peertube Projekt](https://joinpeertube.org) - Förderierte Video Sharing Plattform
- [PeerTube Instanzsuche](https://joinpeertube.org/instances#instances-list) - Interaktive Instanz-Auswahl
- [PeerTube Instanzliste](https://instances.joinpeertube.org/instances) - Übersicht über PeerTube-Instanzen

## Andere Videoplattformen

- [CCC Media](https://media.ccc.de) - Videoplattform des Chaos Computer Club mit einer Vielzahl von Videos aus der Technik-Szene ([Datenschutzerklärung](https://media.ccc.de/about.html#privacy), [Verantwortliche:r](https://www.ccc.de/en/imprint))
