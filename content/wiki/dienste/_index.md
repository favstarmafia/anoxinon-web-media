---
title: Dienste
layout: wikipage
---

Lange schon läuft nicht mehr alles lokal auf dem eigenen PC - das Internet bietet nahezu grenzenlose Möglichkeiten, aber auch Risiken. Von WhatsApp bis zu YouTube ist vieles vernetzt und läuft "online". Da fällt es oft schwer, den Überblick zu behalten, welche der ganzen Onlineangebote vertrauenswürdig sind und verantwortungs- und respektvoll mit Datenschutz und Datensicherheit umgehen und welche nur mit Marketingsprüchen um sich werfen. Die wirklich guten verschwinden oft in der Niesche. Mit dieser Kategorie wollen wir unsere Erfahrungen teilen und die Goldperlen - oder zumindest die bessere Hälfte - im Internet hervorheben.

## Anoxinon Dienste

Gleich vorweg bevor ihr euch in die weiten unserer Instanz- und Dienste-Liste stürzt sei angemerkt, dass wir im Rahmen des Anoxinon e.V. selbst einige Dienste anbieten. Eine Auflistung davon gibt es [auf unserer Webseite](https://anoxinon.de/dienste/). Ein Auszug:

- [Anoxinon Social](https://anoxinon.de/dienste/anoxinonsocial) - Mastodon-Instanz
- [Anoxinon Messenger](https://anoxinon.de/dienste/anoxinonmessenger) - XMPP-Instanz
- [Anoxinon Share](https://anoxinon.de/dienste/anoxinonshare) - Jirafeau-Instanz
- [Anoxinon Mumble](https://anoxinon.de/dienste/mumble) - Audiochat via Mumble

-> Unsere [Datenschutzerklärung](https://anoxinon.de/datenschutzerklaerung/) und das [Impressum](https://anoxinon.de/impressum/) für unsere Dienste.

## Hinweis zur Verwendung der Instanz-Listen

Für Open Source und datenschutzfreundliche Software gibt es mittlerweile einen ganzen Haufen an Internet-Angeboten ganz klassischer Natur oder in der Cloud. Wir listen hier im Wiki die Instanzen und Anbieter:innen auf, die wir für empfehlenswert halten. Dennoch ist es klar, dass manche Angebote - je nach eigenem Anspruch und Bewertungskriterien - besser als andere sind. Das berücksichtigen wir hier jedoch nicht und ordnen die verschiedenen Dienste schlicht alphabetisch. Die letzte Entscheidung liegt dann bei dir, falsch machst du mit keinem der genannten Dienste etwas. Am besten vegleichst du bei Bedarf selber kurz und entscheidest dich dann für den, der dir am meisten zusagt.

### Kriterien

Nachfolgend werden einige Kriterien aufgelistet, die wir beachten, wenn wir Dienste in unser Wiki aufnehmen.

#### Transparenz

Die empfohlenen Dienste sollen wenn möglich eine **aufschlussreiche Datenschutzerklärung** und eine Angabe über die **verantworliche Person** bieten. Das schafft Transparenz über den Umgang mit personenbezogenen Daten und eine Kontaktmöglichkeit, wenn es Unklarheiten oder Differenzen gibt. Die Datenschutzerklärung und die verantworliche Stelle verlinken wir immer gleich mit dem entsprechenden Dienst.

#### Datenschutz

Es sollen nur für den Betrieb der Dienste nötige Daten gesammelt und gespeichert werden, auf Drittanbieter:innen sollte wo es auch geht verzichtet werden. Das geht einher mit den anderen Punkten, die dieses Kriterium unterstützen, möglich und überprüfbar machen.

#### Open Source und Dezentralität

Die Software, die dem Dienst zu Grunde liegt, soll quelloffen sein. Das bedeutet, dass der Quelltext einsehbar und überprüfbar sein soll. Genauso soll er bei Bedarf angepasst werden und auf eigenen Servern betrieben werden dürfen.

#### Föderalität

Wenn möglich sollen die Dienste, wenn sie föderal gestaltet sind, dies auch machen und nicht als "Walled Garden" andere Instanzen ausschließen.



