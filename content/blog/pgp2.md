---
title: OpenPGP - Teil 2 - Der öffentliche Schlüssel
date: "2020-12-13T00:00:00+00:00"
tags:
- Anfänger
categories:
- Freie Software
banner: "/img/thumbnail/openpgp-final.png"
description: Der öffentliche Schlüssel bei OpenPGP spielt eine zentrale Rolle für die Verwendung der Verschlüsselung. In diesem Beitrag geht es um die öffentlichen Schlüssel.
series: pgp
---

Der öffentliche Schlüssel bei OpenPGP spielt eine zentrale Rolle für die Verwendung der Verschlüsselung. Aus diesem Grund werden wir uns in diesem Blogbeitrag auf die Bearbeitung und die Veröffentlichung des öffentlichen Schlüssels konzentrieren.

---

{{< series-parts >}}

---

**Inhaltsverzeichnis:**

1. [Was ist der öffentliche Schlüssel?](#1-was-ist-der-öffentliche-schlüssel)
2. [Muss ich alle Daten veröffentlichen?](#2-muss-ich-alle-daten-veröffentlichen)
3. [Wie kann ich den öffentlichen Schlüssel verteilen?](#3-wie-kann-ich-den-öffentlichen-schlüssel-verteilen)
4. [Brauche ich ein Ablaufdatum?](#4-brauche-ich-ein-ablaufdatum)

---

### 1. Was ist der öffentliche Schlüssel?

Der öffentliche Teil deines OpenPGP-Schlüsselpaars dient zur Verteilung an die Kommunikationspartner. Er wird somit an alle verteilt, weshalb er auch als öffentlicher Schlüssel bezeichnet wird.

In der leserlichen Darstellung sieht ein öffentlicher Schlüssel so aus:

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBF85APYBEADdfJcHzVT4oA6I3Mx+cKwgJUSb/JyFlU7eZVmtBR0fMTnXGUDF
dLNn6MI5QDDLfLtLH+nXgMmftThcTyMuj2j+Lor4ws+uTmrFqYjuVdv7Su8gCg00
Rtp/zxzt/xrIz9JfhvdMYTNAu3xfxOxZ2eYMEYWBj6oeDNhlzsSZq4z4bHJ1KCpc
LV3ljsQhRToLIyLQPCsd8IX0Jjmuw91D7EtIhCyq+9X4H3gZYQoBw72ydSg+w954
UyKUGzap8hDYnSCkVz/Xsk+gRHfTOm95KUVX5LJFS47hZDzSlFRe4AHsnEYLyg5k
bum//aPYmFjSeCl06a3quYaObUVyDu1OHYfN6UTxxjF/s9wCnmwgTsKstu6bngBs
[...]
Z4eWsMBby1sVt7gFHdlGgJd67cwyX7zG7j3cIcMOVKrFL9GsMV6VFwU9ka8uv/3R
sI5SRCpEzCvBPuS+2iEKvohc8ra7SOwmITDAgHH5mJqhWlKwS97LeYhr9aETk9L2
DbrPvqdnF+OnGXLAcjsWTddrEVQF9v866i0oRKk74/uT/EpwqlbeoXhuH6pMBmRE
0omNXSMgrqQKUZ3shY8Z8izsTVZ8quVn9ptS+7K8MCFF+7o83rIWK30Sq4pnj1jd
Pe02NXQoQWq2AiYLy7gLnm/I6QgFg6u3d452smq/+5v6i1TQCKZb3SM9ZehctT6E
SpCvHeZ5ocsqRlZ9A3kw281/y4ZJ2DCapOkoZpK17HVU13kn6hdexfbLZ5u4YpKV
xCh9bXqgXRNzgFyd9Sx4QcEUK89bcBnpbRJF1kd0DZkwaWqGz8VpHY9tztDl6yQP
Vfc6B+cGGQMhNHdPr4PW0RxW7iP2JCoXPQzZlumeSH7yfNImxx+1dI9x0aPvbEQK
axEJbPQUXSt721UIs9V528S9ICMas92KoY54BF5oKGZjRWSv3mviJp98+xxZUA0l
a9Do7NbYdXMr8w8Rgq1PkYe7eyk/Jb1fGVMtw78AQer5ZEjBqQ==
=LjYn
-----END PGP PUBLIC KEY BLOCK-----
```

Der öffentliche Schlüssel enthält jedoch einige Informationen:

```
pub   rsa4096 2020-08-16 [SC] [verfällt: 2022-08-16]
      DF5EB8149E721C161104399D944BE2F3765AEE79
uid        [ ultimativ ] Vornam Nachname <work@domain.tld>
uid        [ ultimativ ] xmpp:user@domain.tld
uid        [ ultimativ ] Mein Nickname <nickname@domain.tld>
sub   rsa4096 2020-08-16 [E] [verfällt: 2022-08-16]
sub   rsa4096 2020-08-16 [A] [verfällt: 2022-08-16]
```

In der ersten Zeile finden wir die Informationen zum Hauptschlüssel. Welcher Typ vom Schlüssel es ist, wann der Schlüssel erzeugt wurde und wann er ablaufen wird, wenn er vom Besitzer nicht vorher verlängert wurde.

In der zweiten Zeile finden wir den Fingerabdruck des Schlüssels. Oft wird dieser etwas aufbereitet, damit er besser gelesen und verglichen werden kann:

```
pub   rsa4096 2020-08-16 [SC] [verfällt: 2022-08-16]
      DF5E B814 9E72 1C16 1104  399D 944B E2F3 765A EE79
uid        [ ultimativ ] Vornam Nachname <work@domain.tld>
uid        [ ultimativ ] xmpp:user@domain.tld
uid        [ ultimativ ] Mein Nickname <nickname@domain.tld>
sub   rsa4096 2020-08-16 [E] [verfällt: 2022-08-16]
sub   rsa4096 2020-08-16 [A] [verfällt: 2022-08-16]
```

Es folgen die Benutzeridentitäten. Der Besitzer kann mehrere Identitäten an den Schlüssel verknüpfen. So kann der Schlüssel beispielweise für die berufliche und private E-Mail Kommunikation verwendet werden oder zum Chatten via XMPP.

Es folgen die Unterschlüssel, auf die wir aber aktuell noch nicht eingehen werden.

Des Weiteren enthält ein öffentlicher Schlüssel auch die Signaturen von anderen Personen, worauf wir noch genauer eingehen werden. Nachdem eine Person (z.B. der Chef) den Schlüssel seines Kommunikationspartners (z.B. der Angestellte) geprüft hat, kann er den Schlüssel unterzeichnen. Die Prüfung eines öffentlichen Schlüssels läuft in der Regel über den Namen und den Fingerabdruck des öffentlichen Schlüssels, der Name wird mit den Informationen aus dem Personalausweis abgeglichen.

```
pub rsa4096 2020-08-16 [SC] [verfällt: 2022-08-16] DF5EB8149E721C161104399D944BE2F3765AEE79 
uid [ ultimativ ] Vorname Nachname work@domain.tld 
sig 3 944BE2F3765AEE79 2020-08-17 Vorname Nachname work@domain.tld 
sig 3 1111111111111111 2019-05-14 [User-ID nicht gefunden] 
sig 3 2222222222222222 2019-05-14 Chef chef@domain.tld 
uid [ ultimativ ] xmpp:user@domain.tld 
sig 3 944BE2F3765AEE79 2020-08-17 Vorname Nachname work@domain.tld 
uid [ ultimativ ] Mein Nickname nickname@domain.tld 
sig 3 N 944BE2F3765AEE79 2020-08-17 Vorname Nachname work@domain.tld 
sub rsa4096 2020-08-16 [E] [verfällt: 2022-08-16] 
sig 944BE2F3765AEE79 2020-08-16 Vorname Nachname work@domain.tld 
sub rsa4096 2020-08-16 [A] [verfällt: 2022-08-16] 
sig 944BE2F3765AEE79 2020-08-16 Vorname Nachname work@domain.tld
```

In diesem Beispiel kann man erkennen, dass die Benutzeridentität work@domain.tld insgesamt 3 Signaturen hat.

![Der GNU Privacy Assistant mit der Schlüsselverwaltung, bei der man am unteren Bildschirmrand die Signaturen eines Schlüssels sieht](/img/pgp/gpa-cert-04.png)

### 2. Muss ich alle Daten veröffentlichen?

Nein, muss man nicht. Die Implementierung GnuPG beispielweise ermöglicht eine Teilmenge der Schlüsselinformationen zu exportieren, damit nicht alle Informationen des Schlüssels verfügbar gemacht werden. Somit ist es möglich nur bestimmte Benutzeridentitäten zu exportieren oder die Signaturen beim Export zu entfernen.

```
gpg --armor --export --export-option export-minimal --export-filter keep-uid='uid=~work@domain.tld' DF5EB8149E721C161104399D944BE2F3765AEE79 | gpg --show-key --with-sig-list

pub   rsa4096 2020-08-16 [SC] [verfällt: 2022-08-16]
      DF5EB8149E721C161104399D944BE2F3765AEE79
uid                      Vorname Nachname <work@domain.tld>
sig 3        944BE2F3765AEE79 2020-08-17  Vorname Nachname <work@domain.tld>
sub   rsa4096 2020-08-16 [E] [verfällt: 2022-08-16]
sig          944BE2F3765AEE79 2020-08-16  Vorname Nachname <work@domain.tld>
sub   rsa4096 2020-08-16 [A] [verfällt: 2022-08-16]
sig          944BE2F3765AEE79 2020-08-16  Vorname Nachname <work@domain.tld>
```

### 3. Wie kann ich den öffentlichen Schlüssel verteilen?

Es gibt mehrere Möglichkeiten den öffentlichen Schlüssel zu verteilen. Jedes Verfahren hat seine Vor- und Nachteile.

1. Speichermedium / E-Mail / Chat: Man kann den öffentlichen Schlüssel ganz normal auf ein Speichermedium (USB-Stick) kopieren, per E-Mail an den Kommunikationspartner senden, per XMPP innerhalb eines Chats verschicken oder über XMPP-PEP veröffentlichen. Nachteil, die Nutzer müssen erst miteinander in Kontakt treten, um die öffentlichen Schlüssel auszutauschen. Vorteil, man braucht so den Schlüssel nicht veröffentlichen.
2. Key-Server: Es gibt zwei Arten von Key-Servern, welche für den Austausch der Schlüssel verwenden werden können. Der öffentliche Schlüssel wird auf einem Key-Server hochgeladen, auf den alle Zugriff haben. Das ist vergleichbar mit einem öffentlichen Adressbuch. Nachteil, jeder kann die Informationen aus dem Schlüssel abfragen. Man sollte sehr gut überlegen, ob man dies möchte oder nicht.
3. Web-Key Directory: Der öffentliche Schlüssel wird in einem definierten Format auf einem Web-Server mit HTTPS abgelegt. Kennt der Kommunikationspartner die E-Mail-Adresse, so kann das Programm anhand der E-Mail-Adresse die abzufragende URL ermitteln und den öffentlichen Schlüssel downloaden.

### 4. Brauche ich ein Ablaufdatum?

Ja, ein Ablaufdatum des Schlüssels ist zu empfehlen. Kommt es zum Verlust des eigenen privaten Schlüssels (Hardware-Defekt, Token verloren, gelöscht), kann man den Schlüssel nicht mehr "widerrufen". Der Schlüssel bleibt somit immer gültig, auch wenn man ihn ohne den privaten Schlüssel nicht mehr nutzen kann. Somit ist es empfehlenswert ein Ablaufdatum (z.B. 1 oder 3 Jahre) zu setzen. Dieses lässt sich einfach verlängern, jedoch muss der öffentliche Schlüssel dann auch wieder "verteilt" werden.
