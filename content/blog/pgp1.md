---
title: OpenPGP - Teil 1 - Einleitung
date: "2020-10-28T00:00:00+00:00"
tags:
- Anfänger
categories:
- Freie Software
banner: "/img/thumbnail/openpgp-final.png"
description: Zum Schutz von Daten vor dem Zugriff von unbefugten Dritten und zur Sicherstellung der Integrität lassen sich diese
  digital signieren und verschlüsseln. Hierfür kann die standardisierte Technologie OpenPGP verwendet werden. In dieser Artikelserie
  werden wir auf Themen rund um OpenPGP eingehen.
series: pgp
---

{{< series-parts >}}

---

**Inhaltsverzeichnis:**

1. [Einleitung](#einleitung)
2. [OpenPGP](#openpgp)
3. [Public-Key-Verschlüsselungsverfahren](#public-key-verschlüsselungsverfahren)
4. [Schlüsselpaar erzeugen](#schlüsselpaar-erzeugen)
5. [Öffentlicher Schlüssel](#öffentlicher-schlüssel)
6. [Ausblick](#ausblick)
7. [Literatur und Quellen](#literatur-und-quellen)

### Einleitung

Zum Schutz von Daten vor unbefugten Dritten und zur Sicherstellung der Integrität lassen sich Daten (wie Dokumente, E-Mails, Chats) digital signieren und verschlüsseln. Hierfür kann die standardisierte Technik *OpenPGP* verwendet werden. In unserer Artikelserie "OpenPGP", werden wir auf Themen rundum *OpenPGP* eingehen.

### OpenPGP

Bei der digitalen Übermittlung von vertraulichen Daten sind zwei wesentliche Aspekte wichtig:

* Sind die Daten vom Absender unverändert?
* Können die Daten nur vom Empfänger gelesen werden?

Ein Beispiel aus der Praxis ist die *E-Mail*. Wie kann der Empfänger einer E-Mail sicherstellen, dass die E-Mail wirklich vom angegebenen Absender ist und sich nicht um Spam handelt? Um sicherzustellen, dass der Inhalt einer E-Mail wirklich von der Person stammt, ist es möglich, Nachrichten digital zu signieren - eine Nachricht digital unterschreiben.

Darüber hinaus können Inhalte von E-Mails sowohl auf den Endgeräten von Dritten gelesen werden, als auch beim jeweiligen Anbieter gelesen und analysiert werden, da die E-Mail i.d.R. ohne zusätzliche Verschlüsselungsebene auf dem Endgerät und auf den Servern des Dienstanbieters gespeichert werden.

Die soeben genannten Probleme können mit OpenPGP gelöst werden, in dem man beispielsweise seine ausgehenden E-Mails digital signiert und/oder verschlüsselt. Das kann mit der standardisierten Technik *OpenPGP* einfach umgesetzt werden.

![Ein Screenshot vom E-Mail-Client Claws-Mail; Es ist eine E-Mail geöffnet, die sowohl verschlüsselt als auch signiert ist](/img/pgp/claws-mail-07.png)

### Public-Key-Verschlüsselungsverfahren

Beim Verschlüsseln von Daten ist die Verwendung eines Passworts die naheliegendste und einfachste Möglichkeit. Alice und Bob - die Standardnamen für zwei Personen in der Kryptographie - treffen sich und machen untereinander ein Passwort aus, welches nur beide kennen. Wenn Alice eine Nachricht an Bob schreibt, verschlüsselt sie die Nachricht mit diesem Passwort. Bob verwendet das gleiche Passwort, um die Nachricht zu entschlüsseln, und kann den Inhalt der Nachricht lesen. Dieses Verfahren wird als "Symmetrisches Kryptosystem" bezeichnet. Leider hat dieses Verfahren einige Nachteile und ist in der Praxis oft schwer umsetzbar, da jedes Kommunikationspartnerpaar ein Passwort vereinbaren und sicher aufbewahren muss. Zusätzlich kann es bei der falschen Umsetzung zu Sicherheitsproblemen führen, wenn eine Nachricht an mehrere Personen gesendet wird und dabei mit jeweils anderen Schlüsseln verschlüsselt wird. Bei OpenPGP verwendet man ein "Asymmetrisches Kryptosystem". Jede Person besitzt ein Schlüsselpaar, welches aus einem öffentlichen und einem privaten Teil besteht.

Auf der Webseite des Bundesamt für Sicherheit in der Informationstechnik wird das in einem [Video](https://www.bsi-fuer-buerger.de/BSIFB/DE/Empfehlungen/Verschluesselung/EMail_Verschluesselung/EasyGPG/EMail_EasyGPG.html) sehr schön veranschaulicht.

Der private Teil des Schlüssels kann verwendet werden, um Daten zu signieren oder Daten zu entschlüsseln. Der öffentliche Teil des Schlüssels kann verwenden werden, um Signaturen zu prüfen und Daten zu verschlüsseln. Der private Schlüssel bleibt immer beim Besitzer und sollte nicht an Dritte weitergegeben werden. Der öffentliche Schlüssel wird den Kommunikationspartnern übermittelt.

Nun können Daten verschlüsselt und signiert werden, ohne explizit vorher ein Passwort zu vereinbaren. Ein wesentlicher Vorteil bei diesem Verfahren ist, dass man nicht für jeden Kommunikationspartner ein eigenes Passwort vereinbaren muss. Jeder Kommunikationspartner verwendet immer *Deinen* öffentlichen Schlüssel.

### Schlüsselpaar erzeugen

Um OpenPGP verwenden zu können, benötigt jeder Kommunikationspartner ein Schlüsselpaar. Ein Schlüsselpaar lässt sich unter anderem auf einem Linux System über die Anwendung `gpa` erstellen.

![ein Screenshot vom GNU Privacy Assistant; hier sind die Schlüsselliste und die Details für einen Schlüssel sichtbar](/img/pgp/gpa-key.png)

Ein Schlüsselpaar hat einen Fingerprint - einen Fingerabdruck - welcher oft auch als Key-ID bezeichnet wird. In unserem Beispiel ist dies `9618 8498 7D7B 7FB2 29C5 C2B8 553F F261 AC20 99F0`.

### Öffentlicher Schlüssel

Der öffentliche Schlüssel ist der Teil, welchen man mit seinem Kommunikationspartner austauscht. Hiermit kann der Kommunikationspartner *Deine* digitalen Signaturen prüfen und E-Mails an *Dich* verschlüsseln.

In der Regel ist der Schlüssel in einer .asc- oder .gpg-Datei gespeichert und für einen Endbenutzer auf den ersten Blick nicht wirklich aussagekräftig.

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQENBF66OPkBCADNtllr/9FyCIOd8dCswVO/O3Ysx3MZVxAvhO8cUE7bqnPDPUb9
MF9hw/P8PVrqaGQf/CXljgIoDha6JXiacEVf+F3iZNjqQahoeM3Dm6mTjvTl4Tzo
Yeohyd10yb+sdX7djoPMx4U2QBfCDQqFuZSZJ4goPxjGkaVKGVX6/rgZ1vwkx0ue
KPvQZD6IryFvxqXjgDTaY3IEStq0H6cfScfi1vHx+OU9laAWV89DKgDEOAdDT+El
G2NzdICW9hAVHkfiwgVjji7mOjgAtxO0TRBJcD5fcFinarhilX67Q9bZcPE05ggE
BS2Rbg1a18s/XbiBHRBt3/SAJIUqKXq+qdbPABEBAAG0JVZvcm5hbWUgTmFjaG5h
bWUgPG1haWxib3hAZG9tYWluLnRsZD6JAU4EEwEKADgWIQSWGISYfXt/sinFwrhV
P/JhrCCZ8AUCXro4+QIbAwULCQgHAgYVCgkICwIEFgIDAQIeAQIXgAAKCRBVP/Jh
rCCZ8KhOB/93TYDiQW4Dhsmd9ZLPrgvCRCEiD81Y+Xeb+lGgyGBS8r6Er/E8LGFI
hvnGfzZvQQ+Q3SWzVGSTz7JGe48js6K+N6AYuNZxAnNagA9LwqdwN/O4ng7OZTHx
lZXnASb5UG9BpHWxAOMm+2Oi+wVdluuBMom++MyfSCQx/zdyo17ahf+KZR+VPyh1
uFM98fDPDWX/iWB2hQ++QqfsO2JAoqfKZXrcRpkyPw8UTp4Io8mvoEgfpaI3wK1h
QnFfH/FdcmDsLXT4d7GbBKhKXhZwAOOXFfo8976UO4K4JBO0uS5PXExVDAKw3p0i
TmBCMIkrtBvawMonOpEN97Ss+QEyGvoquQENBF66OPkBCADNNc/ZSrUli7XtJwDK
j4QDri1H/nBZ9gT5h9NKwTIuWsvheNfZFFjMDh7VuYQSqE3M8foGFApw+WrkEhlf
hRcjIGy5KROpT3s3elkMPan/PUtgvQVFChskVktm/72iSygAwmfyaBBQ1QBPtmDh
HwwK16rXUY7dzP28Z/i1Mi+V+IyoPQ1NYcOyEYA7DnXoGb0vV0U9C+1btUJFvzt2
DkP15nVRwaMSIHcO9pYdZeSEme019rbxnRbKGBqE28/3lKJFOBH6MjUIJY0jS1Um
VF8QUyWSvWMXXLgThpOy7iTRGseLB5e4Dm5THrsv8lc4QTaWxoEj66h1BWkXNNyE
PZtTABEBAAGJATYEGAEKACAWIQSWGISYfXt/sinFwrhVP/JhrCCZ8AUCXro4+QIb
DAAKCRBVP/JhrCCZ8PpBB/0S66MT9+FIkR88IvFgmkJnpfvuM+OHMTfvy8mRyeGB
bgxw8J3WccXKRieCuBsTnfh9QzVNAE1mtS3eeXG7gQr4lMBQQc0Qxhe/CLCRbzvI
82hllNNzihoE5rvFaN2imhcnJWO8UODcZOIS292ztmlYABTy6f5AFZQJAh5WYcw6
QYzNj7ZM2d2Bq+jfgnwzpbw7aIwvZEHNIofc6B63WABp0XY3Mdhrn50ygF8vdCjK
F2lb5Az6CXb/rSIQeINbOKvhKYXTESswJqk6/PNfmFIs35JjBMTiQZQLBo0tIGb/
Ibnv9ONYWGjka6TnXKKvp89Y33d7k1w3dgmjBZVgTIXW
=U0vE
-----END PGP PUBLIC KEY BLOCK-----
```

Erst ein Programm, welches den PGP-Standard implementiert, kann diesen Zeichen eine Bedeutung geben:

```
pub   rsa2048 2020-05-12 [SC]
      961884987D7B7FB229C5C2B8553FF261AC2099F0
uid                      Vorname Nachname <mailbox@domain.tld>
sub   rsa2048 2020-05-12 [E]
```

Hier bei kann man jetzt erkennen, dass das nicht eine zufällige Zeichenfolge ist, sondern ein öffentlicher Schlüssel, der auch personenbezogene Daten enthält.
Es kann ein Name und E-Mail-Adresse und ggf. auch ein Foto oder eine XMPP-Adresse enthalten sein. Daher sollte man einiges bei der Verwendung von öffentlichen Schlüssel beachten.

Da der öffentliche Schlüssel eine zentrale Rolle bei dem asymmetrischen Kryptosystem spielt, sind hierbei zwei Aspekte sehr wichtig:

* Wie kann ich meinen öffentlichen Schlüssel mit meinem Kommunikationspartner teilen?
* Wie kann ich Informationen aus meinem öffentlichen Schlüssel verbergen?
* Wie kann ich die Korrektheit eines Schlüssels meines Kommunikationspartners prüfen?

Es gibt verschiedene Wege, wie die Kommunikationspartner ihre öffentlichen Schlüssel austauschen können. Diese Konzepte haben jeweils ihre Vor- und Nachteile. Hier sollte jeder Benutzer selbst entscheiden, welcher Weg der Richtige für ihn ist.

Wenn man einen Schlüssel von seinem Kommunikationspartner erhalten hat, egal auf welchem Weg, sollte man die Korrektheit des Schlüssels prüfen. Eine sichere Kommunikation kann nur dann gewährleistet werden, wenn die Korrektheit der Schlüssel gewährleistet ist.

### Ausblick

Wir werden in der, mit diesem Artikel begonnenen, Artikelserie die verschiedenen Themenbereiche von OpenPGP erklären.

* Wie veröffentliche ich meinen öffentlichen Schlüssel?
* Wie kann ich OpenPGP in E-Mails verwenden?
* OpenPGP auf einer Smartcard/einem Token
* OpenPGP auf dem Smartphone

### Literatur und Quellen

* https://tools.ietf.org/html/rfc4880
* https://de.wikipedia.org/wiki/Asymmetrisches_Kryptosystem
* https://www.bsi-fuer-buerger.de/BSIFB/DE/Empfehlungen/Verschluesselung/EMail_Verschluesselung/EasyGPG/EMail_EasyGPG.html
