---
title: F-Droid als App-Bezugsquelle
date: "2021-12-02T17:35:00+01:00"
tags:
- Anfänger
categories:
- Freie Software
- Datenschutz
banner: "/img/thumbnail/f-droid_als_app_bezugsquelle.png"
description: Die meisten im Handel zu erwerbenden Android-Smartphones kommen mit dem Google PlayStore als App-Quelle. Wenn man Google meiden möchte und bevorzugt auf freie, quelloffene und datenschutzfreundliche Apps setzen möchte, bietet sich F-Droid als bessere Alternative an. In diesem Artikel stellen wir den F-Droid App-Store vor und führen dich Schritt für Schritt durch die Einrichtung.
---

**Inhaltsverzeichnis:**

1. [Einleitung](#einleitung)
2. [Was ist F-Droid?](#was-ist-f-droid)
3. [Das F-Droid-Projekt & die Leute dahinter](#das-f-droid-projekt-die-leute-dahinter)
4. [F-Droid verwenden](#f-droid-verwenden)
	- [F-Droid installieren](#f-droid-installieren)
	- [Apps installieren](#apps-installieren)
	- [Apps aktualisieren](#apps-aktualisieren)
5. [Für Forgeschrittene: F-Droid Dritt-Paketquellen](#für-forgeschrittene-f-droid-dritt-paketquellen) 
6. [Fazit](#fazit)
7. [Literatur und Quellen](#literatur-und-quellen)


## Einleitung
Die meisten im Handel zu erwerbenden Android-Smartphones kommen von Haus aus mit einer ganzen Reihe von Google-Applikationen und -Services, ganz prominent darunter der Google PlayStore. Fast jede:r Android-Nutzende hat wohl ein Google-Konto und sei es nur für den PlayStore. Denn per Standard hat man keine Wahl - möchte man Apps, soll man ein Google-Konto erstellen. Das birgt jedoch einige Probleme:


1. Man braucht ein Google-Nutzendenkonto und ist daher an Googles Bedingungen gebunden.
2. Das App-Installationsverhalten und die App-Installationen werden von Google nachverfolgt und ausgwertet
3. Die App-Auswahl im Google-Store beinhaltet propritäre und quelloffene Applikationen nebeneinander. Apps zu finden, die quelloffen sind, ist nicht immer ganz einfach, es gibt keinen verlässlichen Filter oder eine Kategorie. Lediglich der Titel oder die Beschreibung kann einen Anhaltspunkt sein.
4. Die App-Auswahl im Google-Store beinhaltet datenschutzfreundliche und datenschutzinvasive Applikationen, ohne es sauber auseinanderhalten zu können
5. Die App-Auswahl im Google-Store ist auch ein beliebtes Ziel für Schadsoftware, wie es sie in der Vergangenheit schon desöfteren gegeben hat


Daher wäre eine alternative App-Bezugsquelle interessant und lohnenswert. Eine solche ist zum Beispiel [F-Droid,](https://f-droid.org) welche wir nachfolgend im Detail beschreiben möchten.

## Was ist F-Droid?
F-Droid ist ein alternativer App-Store **und** App-Katalog für Android. Was F-Droid selbst dazu schreibt:

> F-Droid is an installable catalogue of FOSS (Free and Open Source Software) applications for the Android platform. The client makes it easy to browse, install, and keep track of updates on your device.
> 
> Quelle: [F-Droid Webseite](https://f-droid.org/)

F-Droid besteht aus den folgenden Komponenten. 

**F-Droid App:**
Die F-Droid App ist sozusagen der "App-Store" auf deinem Handy. Mit der F-Droid App kannst du Apps installieren und verwalten, wie mit dem PlayStore auch.

**F-Droid Repository:**
Das F-Droid Repository ist der App-Katalog, der in der F-Droid App zur Verfügung steht. Ganz einfach gesagt eine Liste von Apps, die heruntergeladen und installiert werden kann.

**Weitere F-Droid Repositories:**
Die oben angerissene Unterschiedung zwischen **F-Droid App** und **F-Droid Repository** ist wichtig, denn sie bietet einen entscheidenden Vorteil gegenüber dem PlayStore: Neben dem offiziellen F-Droid Repository kann man weitere Repositories in der F-Droid App hinzufügen, die jeweils weitere Apps anbieten können! Prinzipiell kann jede:r so ein Repository mit beliebigen Apps anbieten.

## Das offizielle F-Droid Repository
Das standardmäßig mit F-Droid eingerichtete Repository ist das offizielle vom F-Droid-Projekt. 

**Inhalt des Repos:**

Es enthält ausschließlich **quelloffene** Apps. Die Apps werden außerdem selbständig vom F-Droid Team gebaut → das bedeutet, die Installationsdatei wird vom F-Droid Team aus dem Quelltext erzeugt, damit sichergestellt ist, dass nur genau dieser Quelltext ausgeliefert wird. 

Eine Ausnahme bieten sogenannte reproduzierbare Builds, bei diesen wird die reproduzierbare Installationsdatei ausgeliefert. Das bedeuet, dass man prüfen kann, ob das Bauen des Quelltexts die gleiche Installationsdatei liefert, wie die von den Entwickelnden zur Verfügung gestellte und kann so sichergehen, dass genau dieser Quelltext ausgeliefert wird. 

Den Quelltext der Apps selbst kann man bedingt durch die Quelloffenheit selbstverständlich auch herunterladen.

**Qualitätsmerkmale der aufgenommenen Apps und Transparenz:**

Wie beschrieben werden ausschließlich quelloffene Apps zur Verfügung gestellt. Es findet aber noch eine weitere Unterscheidung statt. Der Idealzustand ist, dass die komplette angebotene Software zusätzlich vollständig **frei wie in Freiheit und ohne Tracker** ist. Ist eines der Merkmale nicht gegeben, wird das durch die sogenannten "Anti-Features" in der App-Übersicht bekanntgegeben. Folgende Antifeatures gibt es:

* **Werbung:** Die App beinhaltet Werbung
* **Tracking:** Die App beinhaltet Tracking → meldet deine Nutzendenaktivität irgendwohin. Das Antifeature wird vergeben, auch wenn das Tracking abgeschaltet werden kann. Ist es per Standard aus, wird es nicht vergeben.
* **Unfreie Netzwerkdienste:** Die App hängt von unfreien Netzwerkdiensten ab oder untersützt diese. Das bedeutet, dass die App zum Funktionieren bzw. für einige Funktionen Internetdienste braucht, die nicht quelloffen und/oder nicht frei sind. 
* **Unfreie Addons:** Unterstützt unfreie Apps oder Plugins.
* **Unfreie Abhängigkeiten:** Braucht unfreie Applikationen zum Funktionieren. (Bspw. Google Maps.)
* **Unfreies Upstream:** Der "upstream" Quelletext ist unfrei. Das bedeutet, dass die "Originalapp" um diese unfreien Teile bereinigt wurde und diese bereinigte Version in F-Droid angeboten wird.
* **Unfreie Inhalte:** Enthält unfreie Inhalte, die kein Quelltext sind. Beispielsweise Bilder, Audio, Video.
* **Bekannte Sicherheitslücke:** Die App enthält eine bekannte Sicherheitslücke
* **Deaktivierter Algorithmus:** Die App wurde mit einem unsicheren Algorithmus signiert.
* **Kein Quelltext seit:** Der Quelltext steht nicht mehr zu Verfügung, weswegen es keine Updates mehr geben wird.


## Das F-Droid Projekt & die Leute dahinter
Hinter F-Droid steht die gemeinnützige, britische Organisation F-Droid Limited. Das Projekt wird vollständig von Freiwilligen gestemmt. Alle Inhalte von F-Droid sind frei und quelloffen. Dazu zählt die Webseite, die Grafiken und das Logo sowie die Android-App und Repository-Software. Das F-Droid Projekt selbst findet man folgend:

* [Webseite](https://f-droid.org)
* [F-Droid Dokumentation](https://f-droid.org/en/docs/)
* [F-Droid Forum](https://forum.f-droid.org/)
* [XMPP-Gruppenchat](xmpp:#fdroid#matrix.org@bridge.xmpp.matrix.org?join)
* [Matrix-Raum](https://matrix.to/#/#fdroid:f-droid.org)



## F-Droid verwenden

### F-Droid installieren
Alles, was man machen muss, um F-Droid einzurichten, ist die F-Droid-App zu installieren. Da die App nicht im Google PlayStore verfügbar ist (Google mag keine alternativen AppStores) muss man die Installationdatei von der Webseite herunterladen und installieren. Standardmäßig verbietet Android das, man wird aber bei der Installation gefragt, ob man es erlauben möchte. Keine Angst, F-Droid ist für Google zwar "unbekannt", aber nicht "böse", wie Android gerne versucht, den Eindruck zu erwecken! 

{{< bildstrecke f-droid_installieren >}}

### Apps installieren
**Apps endecken**
Bei F-Droid gibt es inzwischen hunderte von quelloffenen Apps und es werden immer mehr. Es finden sich echte Goldperlen unter dem reichhaltigen App-Angebot. Daher lohnt es sich, einfach mal in der App-Auswahl zu stöbern. Um Apps aufzufinden, gibt es neben der gezielten Suche verschiedene Wege:

{{< bildstrecke f-droid_apps_entdecken >}}

**Apps installieren**
Wenn du eine App installieren möchtest, funktioniert das sehr ähnlich wie zum Google PlayStore. App heraussuchen - installieren klicken - fertig! Nachfolgend nochmal im Detail mit Bildschirmfotos beschrieben.

{{< bildstrecke f-droid_apps_installieren >}}

### Apps aktualisieren
**App-Aktualisierungen gehen bei F-Droid standardmäßig nicht automatisch!** Es ist daher wichtig, dass du deine Apps selbst aktuell hälst. Bei verfügbaren Aktualisierungen bekommst du eine Benachrichtigung. Genauso kannst du über den Tab "Aufgaben" in der F-Droid App die verfügbaren Aktualisierungen einsehen. Du musst due Updates dann über Druck auf "Installieren" einzeln anwenden.

{{< bildstrecke f-droid_apps_updaten >}}

## Für Forgeschrittene: F-Droid Dritt-Paketquellen
Da F-Droid vollständig quelloffen ist, steht es jedem und jeder frei eigene F-Droid-Repositories mit weiteren Apps zur Verfügung zu stellen. Neben dem offiziellen F-Droid Repository, das standardmäßig aktiviert ist, gibt es eine ganze Reihe an weiteren Repositores. Diese dienen verschiedenen Zwecken, zum Beispiel können sie Apps enthalten, die nicht den Qualitätsansprüchen von F-Droid gerecht werden, deren Aufnahme noch Anpassungen an der App erfordern, die propritär sind oder die aus anderen Gründen nicht in F-Droid zu finden sind. 

**Qualitätskriterien bei Drittquellen**

Die Drittquellen sind von dem offiziellen F-Droid Repositorium und dem F-Droid Projekt unabhängig. Das bedeutet, dass die Apps nicht vom F-Droid Projekt verfiziert und geprüft werden und auch, dass nicht zwingend die gleichen Ansprüche an die App-Qualität, speziell hinsichtlich Quelloffenheit und Datenschutz, gestellt werden. 
Du solltest dich also vor dem Hinzufügen einer Drittquelle also genau informieren von wem sie angeboten wird, was für Apps sie enthält und was du von den Apps erwarten darfst. Eine Installation aus einer weiteren Paketquelle ist vergleichbar mit dem Herunterladen einer App von der entsprechenden Webseite. Man sollte sich überlegen, inwiefern man der Quelle vertraut und welche Berechtigungen für die App plausibel sind. Genauso wie es eine schlechte Idee ist unbekannte E-Mail-Anhänge zu öffnen, ist es eine schlechte Idee unbekannte Drittquellen hinzuzufügen.

Für fortgeschrittene Anwender:innen bietet die Funktion, zusätzliche Paketquellen hinzuzufügen, aber interessante Möglichkeiten. Das wollen wir dir nicht vorenthalten. Denkbar wäre zum Beispiel, sich die benötigten Apps automatisiert selbst bauen und in einer eigenen Paketquelle auf das Handy zu transferieren. Oder eine unternehemseigene Paketquelle für unternehmensinterne Applikationen. Aber auch der Dezentralität kann das zu Gute kommen und ermöglicht beispielsweise das Betreiben eines eigenen Spiegels des offiziellen F-Droid Repositoriums im Heimnetzwerk für die Familie und/oder Freunde.

Zusätzliche Paketquellen werden nativ von der F-Droid App unterstützt, du kannst beliebige weitere F-Droid Repositores hinzufügen:

{{< bildstrecke f-droid_paketquellen_hinzufuegen >}}


## Fazit
F-Droid ist eine wunderbare App-Quelle, wenn man beides - quelloffene Applikationen und transparenten Datenschutz - schätzt. An beide Kriterien stellt F-Droid die höchsten Ansprüche, geht mit gutem Beispiel voran und ermöglicht es dir, deine App-Auswahl bewusster und besser zu gestalten. Es finden sich viele empfehlenswerte Anwendugnen im F-Droid, auch die meisten derer, die wir bei uns auf Anoxinon Media vorstellen, sind im F-Droid zu finden, teilweise sogar exklusiv. Einmal eingerichtet, kann man sich so wunderbar einem großen und beständig wachsenden Katalog an interessanten Apps bedienen, ohne von Google abhängig zu sein.


## Literatur und Quellen
- <https://f-droid.org>
- <https://en.wikipedia.org/wiki/F-Droid>
- <https://android.izzysoft.de/articles/named/fdroid-intro-1>
- <https://mobilsicher.de/ratgeber/so-installieren-sie-den-app-store-f-droid>
- <https://www.kuketz-blog.de/f-droid-freie-und-quelloffene-apps-take-back-control-teil5/>
- <https://www.kuketz-blog.de/empfehlungsecke/#android>
 

