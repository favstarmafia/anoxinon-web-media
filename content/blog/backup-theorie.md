---
title: Backups - theoretische Grundlagen
date: 2021-11-04T11:30:00+00:00
tags:
- Anfänger
banner: "/img/thumbnail/backup.png"
description: >
  Backups rücken oft genau dann in den Fokus, wenn welche benötigt
  werden aber nicht vorliegen. In diesem Artikel gehen wir darauf ein, warum man
  übeerhaupt Backups macht und welche Hintergründe man berücksichtigen sollte. 
---

An Backups denkt man spätestens dann, wenn man sie braucht. Das Problem ist natürlich,
dass man sie schon vorher erstellen muss, damit die dann verfügbar sind, wenn es soweit
ist und man darauf zurückgreifen möchte. Natürlich gibt es Programme und Dienste, die
einfache und schnelle Lösungen versprechen. Die einfache
1-mal-klicken-und-sorgenfrei-Leben-Lösung gibt es allerdings nicht.

## Ursachen für Datenverluste

Zunächst betrachten wir mal eine kurze Liste, was alles zum Datenverlust führen kann:

- Bedienfehler
  - falsche Dateien gelöscht
- Softwarefehler
  - beim Ändern tritt ein Fehler auf und danach gibt es weder die alte noch die neue Version
  - ein Fehler im Betiebssystem zerstört das Dateisystem
- Hardwarefehler
  - Festplatte/SSD hört durch Alterserscheinungen auf zu funktionieren
- Verlust der Hardware
  - selbstverschuldet durch das Vergessen im öffentlichen Raum
  - nicht selbstverschuldet durch Einbrecher
- Umweltfaktoren
  - Spannungsspitze im Stromnetz zerstört die Hardware
  - Wohnungsbrand/Hochwasser zerstört die Hardware
- Schadsoftware
  - "Verschlüsselungstrojaner"

Nicht alle Einträge der Liste sind gleich wahrscheinlich. Die Risiken sind auch von den
konkreten Umständen abhängig: eine alte HDD, die schlecht behandelt wurde hat sicherlich
ein höheres Ausfallrisiko als ein neueres Exemplar. Wer gerne ausführbare E-Mail-Anhänge
öffnet wird auch eher mal einen Verschlüsselungstrojaner in live erleben können.

Wichtig ist: Egal wie vorsichtig man ist oder wie günstig die Umstände sind: Die Risiken
werden nie auf null sinken.

## Ich mache doch nichts Wichtiges am Computer!

Man kann durchaus glauben, dass ein Datenverlust kein Problem darstellt. Wenn es dann zu
einem Datenverlust kommt wird man schnell eines besseren belehrt: alte Fotos, Bookmarks
zu mühsam gefundenen Internetseiten, gespeicherte Passwörter oder auch Spielstände - all
das gibt es.

Kein Computer bedeutet hier keine Ausrede: Wenn man alles per Smartphone oder Tablet-Computer
macht dann liegen dort die wichtigen Dateien, die man nicht unbedingt verlieren möchte.

## Verschiedene Datenträgertypen

Sicherungen und das zugehörige Original sollten möglichst auf ungleichen Datenträgern liegen.
Wenn eine bestimmte Bauart durch eine bestimmte Eigenschaft in bestimmten Situationen ausfällt,
dann ist es von Vorteil, wenn dass nicht das Original und die Sicherungen genau im gleichen
Moment trifft. Der gleiche Grund sorgt dafür, dass die Datenträger nicht zusammen angeliefert
werden sollten, damit nicht beide die gleichen Transportschäden erleiden.

## Georedundanz

Die Umweltfaktoren machen es erforderlich, Kopien an verschiedenen Orten zu speichern. Das
meint aber nicht, dass man einen Laptop auf einem Schreibtisch und eine externe Festplatte mit
der Kopie im Keller liegen hat. Wenn das Haus brennt sieht es für beide Exemplare schlecht aus.
Je größer der physikalische Abstand der Kopien ist, desto besser.

## Offline-Backup

Die Bedienfehler und Schadsoftware verbieten es wiederum, dass das Backup immer erreichbar ist.
Wenn man [versehentlich](https://www.cultofmac.com/257976/bitcoin-hoax-dupes-apple-users-destroying-macs/)
alles löscht und die externe Festplatte mit den Sicherungen angeschlossen ist, dann sind die wohl
oder übel auch gelöscht.

Man braucht also eine Kopie, die nur genau dann erreichbar ist, wenn man ein Backup erstellt oder
man Daten wiederherstellen möchte. Das bedeutet wiederum, dass die Backups nicht vollständig
selbstständig durchgeführt werden können und die Mithilfe des Benutzers benötigt wird.

## Zeitplanung

Wer nach dem Lesen des Artikels panisch wird, sich eine Festplatte kauft, alles was er hat darauf
kopiert und diese dann weglegt, der hat nicht viel gewonnen. Warum? Alles, was seit der letzten
Sicherung gemacht wurde, ist nicht gesichert. Wenn der Rechner ein Jahr später defekt ist, dann
sind alle aktuellen Dateien weg.

Hier muss man also abwägen, wie viel man zu verlieren hat und wann das nächste Backup spätestens
stattfinden soll. Umgekehrt sinkt die Nützlichkeit vom Offline-Backup, wenn es einmal in der Stunde
angeschlossen wird. Man kann sich auch überlegen, ob man Sonderbackuptermine, z.B. vor Reisen,
einführt.

## Qualitätssicherung

Dass man ein Backup erstellt hat bedeutet erst einmal Nichts. Wer sagt, dass sich das am Ende
auch wiederherstellen lässt oder das die wichtigen Dateien darin überhaupt enthalten sind?

Einen Wiederherstellungstest machen einige Backuptools selbstständig bei der Erstellung einer
neuen Sicherung. Ob das Wiederhergestellte benutzbar ist und ob alles, was wichtig ist, im
Backup enthalten ist, kann kein Programm für den Benutzer prüfen. Da muss man selbst tätig
werden: Nicht nur nach der Einrichtung; auch später muss man immer wieder prüfen, ob immer
noch alles so funktioniert wie man es sich gedacht hat, z.B. wenn man wichtige Dateien
irgendwann mal in einem Ordner speichert, den man bei der Einrichtung für unwichtig hielt
und deshalb von der Sicherung ausgenommen hat.

## Verschlüsselung

Auch an dem Verschlüsselungsthema kommt man beim Backup nicht vorbei und auch hier muss man sich
gut überlegen, ob und mit welchem Schlüssel man das Backup verschlüsselt.

Wenn man eine Verschlüsselung verwendet, dann muss man regelmäßig sicherstellen, dass man den
Schlüssel kennt oder so abgelegt hat, dass man bei Bedarf einen Zugang dazu hat, auch nach einem
Datenverlust.

Ein Backup, dass keine personenbezogene oder anderweitig sensible Daten enthält, ist eine Seltenheit.
Wenn man also auf die Verschlüsselung verzichtet, dann sollte man sich gut überlegen, wo man es
lagert und wie dieser Ort abgesichert ist.

## Ein Wort zur Wolke

Eine Wunderlösung ist die Speicherung von Backups in Online-Speichern nicht. Es kann sein, dass
der Online-Speicher alte Dateiversionen vorhält und deren Löschung verhindert, aber das muss
nicht so sein. Es besteht also das Risiko, dass Daten im Online-Speicher durch einen Bedienfehler
des Benutzers oder Schadsoftware beim Benutzer gelöscht werden. Man sollte sich daher gut überlegen,
ob ein Online-Speicher als einziger Ort für eine Datensicherung ausreichend ist oder eher nicht.

Der Mehrwert durch die Möglichkeit, jederzeit und von überall auf die eigenen Dateien zuzugreifen,
kostet oft eine monatliche Gebühr. Hier sollte man sich überlegen, ob eine externe Festplatte, bei
der man unter 100 Euro schon Speicherplatz im Terrabyte-Bereich bekommt, langfristig günstiger ist.

Neben dem Preis hat man eine eigene Festplatte auch deutlich besser unter Kontrolle als einen
gebuchten Speicher in einem fernen Rechenzentrum.

## Synchronisationstools als Teil einer Backupstrategie

Eine Synchronisation ersetzt kein Backup. Ein Backup dient der Wiederherstellung nach einem
Unfall. Wenn nun der Unfall auf alle anderen Geräte synchronisiert wurde, dann hat man Nichts
gewonnen. Eine Synchronisation hilft (wie auch eine Spiegelung über mehrere Datenträger) dann,
wenn ein Datenträger plötzlich vollständig ausfällt, weil man dann noch mindestens eine weitere
Kopie hat. Löscht man hingegen die falsche Datei, dann wird auch die Löschung synchronisiert.

Die Verwaltung von Backups kann allerdings dadurch erleichtert werden, dass man per Synchronisationstool
alles auf einen Computer speichert und dort dann zentral ein Backup erstellt wird. Dann muss man nur
noch einen Computer sichern.

## Snapshots als Teil einer Backupstrategie

Ein Snapshot ist eine Funktion vom Dateisystem oder einer darunter liegenden Ebene, mit der
eine "Kopie" des aktuellen Zustandes erstellt werden kann. Schlaue Snapshotfunktionen arbeiten
nach dem Copy-on-Write-Prinzip. Das Snapshot ist dabei keine Kopie, sondern ein Hinweis an
das System, dass bei jeder Änderung die alte Version aufgehoben werden soll und die neue
Version an eine andere Stelle geschrieben werden soll. Das spart Speicherplatz und ermöglicht
das Aufbewahren von vielen Snapshots, aber ist eine schlechte Grundlage für ein Backup, da
die Daten danach weder mehrfach auf einem Datenträger noch auf mehreren verschiedenen
Datenträgern liegen.

Als Ergänzung und Unterstützung von Backups eignen sich Snapshots wiederum.
Wenn nur ein Bedienfehler gemacht wurde und dieser über der Ebene erfolgt ist,
die die Snapshots verwaltet, dann kann man die verlorenen Daten aus einem
Snapshot wieder hervorholen. Beim Erstellen eines Backups ist als Quellordner
ein Snapshot von Vorteil, weil sich dieses nicht während der Erstellung des Backups
verändert. Damit kann man z.B. nach dem Anlegen eines Backups prüfen, ob die Quelle
und das Ziel die gleichen Daten enthalten, ohne dass in der Zwischenzeit durchgeführte
Änderungen das Ergebnis verfälschen.

## Unvollständige Backups?

Bei den Snapshots gibt es das Copy-on-Write, und bei Backups lässt sich etwas ähnliches machen.
Dann hat man kein Full-Backup sondern ein inkrementelles oder differentielles Backup.
Abhängig von der verwendeten Variante sichert man dann den Unterschied verglichen zum
vorherigen Backup (inkrementell) oder zum vorherigen vollständigen Backup (differentiell).

In beiden Fällen sollte man irgendwann wieder einmal ein vollständiges Backup anlegen.
Entweder, damit das nächste Backup wieder kleiner wird, oder, damit man keine zu
lange "Backupkette" hat, die keine defekten Stellen aufweisen darf und die bei einer
Wiederherstellung vollständig abgearbeitet werden muss. Denn eins ist wichtig: Das
Backup sollte nicht nur vorhanden, sondern auch in einer angemessenen Zeit
wiederherstellbar sein, sonst erfüllt es seinen Zweck nicht.

## Langzeitarchivierung

Backups belegen Speicherplatz. Nun könnte man auf die Idee kommen, dass man alle Backups,
die älter als zwei Wochen sind, wieder überschreibt, weil man dann ja genug Zeit zur
Wiederherstellung hatte. Das funktioniert bei Daten mit denen man täglich arbeitet -
hier bemerkt man schnell, wenn etwas fehlt. Das ist aber nicht anders als bei der
Speicherhierarchie im Rechner - es gibt meistens wenig Daten die man oft benutzt und
viele Daten, die man selten benutzt. Es wird nicht immer wahrgenommen, wenn Daten
verloren gehen: Defekte Speicherzellen oder Sektoren betreffen nun einmal nicht alle
Dateien und nicht jeder Bedienfehler fällt sofort auf.

Die Lösung sind verschiedene Backupstufen mit anderer Aufbewahrungsdauer. Für die
häufig verwendeten und bearbeiteten Dateien mag ein tägliches Backup sinnvoll sein,
und das ist nach zwei Wochen schon wieder völlig obsolet. Nun kann man aber zusätzlich
ein wöchentliches, monatliches und sogar jährliches Backup erstellen, dass man länger
aufbewahrt. Klar, Dateien die man ständig ändert lassen sich daraus nicht
wiederherstellen, aber selten betrachtete Dateien sind so auch nach längerer Zeit
noch wiederherstellbar.

## Fazit

Backups sind nervig aber dennoch wichtig. Das Wichtigste dabei ist es, einen Überblick zu haben:
über die Bedrohungen, vor denen man sich schützt, die Daten, die man sichert, und auch über die
eigene Strategie, die man zur Sicherung verwendet.
