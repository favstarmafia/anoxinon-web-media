---
title: Sichere Logins - Teil 3 - Zwei-Faktor Authentifizierung
date: "2021-09-01T00:00:00+00:00"
tags:
- Anfänger
categories:
- Sicherheit
banner: "/img/thumbnail/sichere_logins_01.png"
series: sichere_logins
description: Logins sind ein elementarer Bestandteil in der digitalen Welt von heute. Was leider auch ein Bestandteil davon ist, sind immer wieder Schlagzeilen über geknackte Konten. Auch weil Endnutzerinnen und Endnutzer unsichere Logins nutzen. Wie Du das vermeiden kannst, wird Dir in diesem Beitrag verständlich erklärt. 
---

# Sichere Logins - Teil 3 - Zwei-Faktor-Authentifizierung

**Inhaltsverzeichnis:**

1. [Einleitung](#einleitung)
2. [Zusätzliche Absicherung von Logins](#zusätzliche-absicherung-von-logins)
3. [Arten von zweiten Faktoren](#arten-von-zweiten-faktoren)
4. [Zeitbasierte Einmalkennwörter](#zeitbasierte-einmalkennwörter)
5. [Grenzen von TOTP/HOTP](#grenzen-von-totp-hotp)
6. [Die Praxis zur Theorie - zeitbasierte Einmalkennwörter verwenden](#die-praxis-zur-theorie-zeitbasierte-einmalkennwörter-verwenden)
7. [Fazit](#fazit)

## Einleitung

Die Bildschirmsperre beim PC und Laptop oder die Anmeldung beim E-Mail-Konto, beim Online-Banking oder beim Online-Shopping - heutzutage nutzt fast jeder unzählige digitale Geräte und Dienste, bei denen man sich in irgendeiner Weise einloggen - authentifizieren - muss. Das ist auch gut so, denn nur so kann sichergestellt werden, dass Deine digitalen Dienste nicht von anderen in Deinem Namen missbraucht werden. Logins sind ein elementarer Bestandteil in der digitalen Welt von heute. Was leider auch ein Bestandteil davon ist, sind immer wieder Schlagzeilen über geknackte Konten und den Schabernack, den Kriminelle damit treiben. Warum? Weil entweder Diensteanbieterinnen und Diensteanbieter ihre Dienste schlecht absichern oder die Endnutzerinnen und Endnutzer unsichere Logins nutzen. Oft ist das keine Absicht, sondern schlicht Unwissen. Gegen ersteres kannst Du nur begrenzt etwas machen, gegen letzteres schon. Genau in diese Lücke zielt diese Artikelserie - Du wirst merken, es ist im Grunde kein Hexenwerk ausreichend sichere Logins zu nutzen.

## Zusätzliche Absicherung von Logins

In den letzten beiden Artikeln hatten wir besprochen, wie man [sichere Passwörter erstellt](/sichere_logins_01/) und wie man [diese komfortabel und sicher verwaltet](/sichere_logins_02/). Dennoch ist es nicht völlig ausgeschlossen, dass das Kennwort eben doch unbeabsichtigt abhanden kommt. Und sei es nur durch eine gefälschte E-Mail (Phishing E-Mail) von Kriminellen - dann schützt auch das beste Kennwort nicht vor unbefugtem Zugriff. Daher stellt sich unweigerlich die Frage, ob man die Gefahr bzw. die Auswirkung die ein Passwortverlust hat, **reduzieren** kann. 

Die Antwort auf diese Frage ist, neben dem Passwort noch einen **zweiten Faktor** für die Authentifizierung bei Logins zu verwenden. Das bedeutet, neben dem Passwort ist noch ein weiterer Faktor für die Anmeldung notwendig. Idealerweise ist dieser Faktor von anderer Kategorie als ein Passwort. Konkret: Das Passwort ist **Wissen*. Der/die Benutzer:in wird durch **Wissen** und zwar durch das **Wissen über das Passwort** identifiziert und bekommt so Zugang zum System. Für einen guten zweiten Faktor bietet sich also zum Beispiel **Besitz** an. Zusätzlich zum Wissen, muss man nachweisen, dass man etwas bestimmtes **besitzt**, um sich in das System einwählen zu können.

Wie so etwas aussehen kann, wollen wir nachfolgend besprechen.

##  Arten von zweiten Faktoren

Nun gibt es verschiedene Arten von zweiten Faktoren. Es gibt spezielle Token, zum Beispiel in Form von USB-Sticks, die man als zweiten Faktor verwenden kann (bspw. Smartcards). Auch der eigene Rechner kann als zweiter Faktor dienen, dann ist nur eine Anmeldung von zuvor erlaubten Systemen möglich (bspw. mittels FIDO). Genauso ist es denkbar, ein biometrisches Merkmal als zweiten Faktor zu verwenden. Den eigenen Fingerabdruck oder das Gesicht zum Beispiel. 

Doch die im Privatumfeld am weitesten verbreitete Art ist wohl **OTP** - das sind Einmalkennwörter, die man für den Login verwendet. Auf diese Methode möchten wir uns hier konzentrieren, denn sie ist einfach einzurichten und zu verwenden, mit hausüblichen Mitteln wie einem Smartphone oder einem anderen Rechner. Speziell die Protokolle **TOTP** und **HOTP** sehen wir uns an.

## Zeitbasierte Einmalkennwörter

Sowohl **TOTP**, als auch **HOTP** sind Verfahren, um zeitbasierte Einmalkennwörter zu erzeugen. Man koppelt sein Login mit einem geheimen Schlüssel, der zum Beispiel von einer Smartphone-App eingelesen wird. Auf dieser Basis wird mathematisch mithilfe des Zeitstempels oder eines Zählers ein sich regelmäßig änderndes Einmalkennwort erzeugt, das man beim Login eingeben muss. Danach ist das Einmalkennwort wertlos. Wenn ein:e Angreifer:in dein Passwort und das Einmalkennwort abfängt, ist das Einmalkennwort schon nach kurzer Zeit, i.d.R. wertlos und es kann kein Nutzen daraus gezogen werden, es ist kein unbefugter Login möglich.

## Grenzen von TOTP/HOTP

Wer aufmerksam mitgelesen hat, wird sich erschließen können, welche Grenzen das mit sich bringt. 

1. Die Systeme (Webseiten...), bei denen du dich einoggst, müssen das Verfahren unterstützen. Ist das nicht der Fall, kannst du es nicht verwenden. Glücklicherweise ist das immer öfter der Fall.

2. Es schützt nur Angreifer:innen, die das Kennwort von dir oder von dritter Stelle abfangen. Wenn der/die Anbieter:in selbst ein Datenleck hat, kann es aber nicht garantiert/unbedingt schützen - sofern der geheime Schlüssel für den zweiten Faktor unter den Daten ist, kann das Einmalkennwort vom Eindringling problemlos selbst generiert werden. Im Worst-Case kann aus dem geleakten Datensatz noch das zugehörige Passwort ermittelt werden, oder ist, schlimmer noch, schon im Klartext dort enthalten. 

3. Auch schützt es nicht, wenn der/die Angreifer:in unentwegt auf den Erfolg des Angriffs wartet und die abgefangenen Zugangsdaten direkt eingibt, wenn das Einmalpasswort noch Gültigkeit hat.

## Die Praxis zur Theorie - zeitbasierte Einmalkennwörter verwenden

**Grundlegend:**

Die einfachste Möglichkeit, zeitbasierte Einmalkennwörter zu verwenden, ist via **Smartphone-Applikation**. Man lädt sich die App herunter und kann dort bei Aktivierung der Zwei-Faktor-Authentifizierung einen QR-Code scannen. Dieser enthält den geheimen Schlüssel, damit kann die App im Hintergrund das Einmalpasswort generieren. Als Resultat musst du die App lediglich für den Login starten, dass Einmalkennwort ablesen und eingeben - fertig.

Es gibt auf dem Markt einige Lösungen solcher Apps. Sehr bekannt ist der Google Authenticator, dieser wird auch oft bei der Einrichtung von Zwei-Faktor-Authentifizierung bei einschlägigen Diensten beworben. Aus Datenschutz- aber auch aus Komfortgründen halten wir diese Lösung nicht für empfehlenswert. Denn: Wenn zeitbasierte Einmalkennwörter auf Basis von TOTP/HOTP verwendet werden, dann kann jede App, die diese Verfahren unterstützt, verwendet werden. Das ist meistens der Fall, auch wenn manche Anbieter:innen partout die Zwei-Faktor-Authenfizierung nicht anhand des Verfahrens, sondern anhand des Produkts "Google Authenticator" vermarkten. Lass dich davon nicht ködern.

### Android-App - andOTP

Auf Android bietet sich die App **[andOTP](https://play.google.com/store/apps/details?id=org.shadowice.flocke.andotp)** für die Zwei-Faktor-Authentifizierung an. Die App ist quelloffen und kann das, was man von einer Zwei-Faktor-App erwarten würde: Die zeitbasierten Einmalkennwörter von beliebig vielen Konten generieren. Ebenfalls kann man die hinzugefügten Konten sichern und wieder importieren. Das ist wichtig, sollte man das Smartphone verlieren oder auf ein neues umziehen. Man kann andOTP wahlweise im [Google Play Store](https://play.google.com/store/apps/details?id=org.shadowice.flocke.andotp) oder im [F-Droid](https://f-droid.org/packages/org.shadowice.flocke.andotp/) herunterladen.

![andOTP App Hauptbildschirm](/img/sichere_logins_03/andotp_01.png)

Die Verwendung ist selbsterklärend: 

- Mit dem Plus-Symbol in der unteren Ecke lassen sich neue Konten hinzufügen, wahlweise per QR-Code-Scan oder manuell
- Die App zeigt jedes Konto in einer Liste an, mit einem Tipp darauf wird das Einmalkennwort zur Eingabe angezeigt

### iOS-App 

Für iOS gibt es [RavioOTP](https://apps.apple.com/de/app/raivo-otp/id1459042137#?platform=iphone) für die Zwei-Faktor-Authentifizierung. Sie bietet die Generierung von zeitbasierten Einmalkennwörtern für beliebig viele Konten und wahlweise ein verschlüsseltes Backup in die iCloud an.

![RavioOTP](/img/sichere_logins_03/raviootp_01.png)

Die Verwendung ist selbsterklärend:

- Mit dem Plus-Symbol in der oberen rechten Ecke lassen sich neue Konten hinzufügen
- Jedes hinzugefügte Konto wird von RavioOTP aufgelistet und das zugehörige, aktuelle Einmalkennwort angezeigt

### Zwei-Faktor-Authentifizierung aktivieren

Nun sollte man hergehen und bei so vielen Konten wie möglich die Zwei-Faktor-Authentifizierung aktivieren. Meist findet sich die Option in den Konto- oder Sicherheitseinstellungen des Services, eine Suche im Internet hilft an der Stelle weiter. Dann kann man einfach den QR-Code mit andOTP oder RavioOTP scannen und künftig, wenn man sich einloggt, das von der App angezeigte Einmalkennwort zusätzlich zum Passwort eingeben.

## Fazit

Wie in den vorhergegangenen Ausführungen gezeigt, ist es nicht wirklich schwierig Zwei-Faktor-Authentifizierung in Form von zeitbasierten Einmalkennwörtern zu verwenden. Für ein bisschen Mühe, bekommt man ein gutes Maß an Sicherheit bei den eigenen Logins obendrauf - eine Prämie, die man gut und gerne mitnehmen kann. In Kombination mit den vorherigen Artikeln zu [sicheren und merkbaren Passwörtern](/blog/sichere_logins_01/) sowie zu [Passwortmanagern](/blog/sichere_logins_02) bist du nun in der Lage, recht einfach und komfortabel deine Logins relativ sicher zu gestalten. Wir können dir nur raten, es einfach mal auszuprobieren. Wenn du also die vorhergegangenen Artikel noch nicht gelesen hast, ist nun der Zeitpunkt das nachzuholen. Es ist ein Zeitinvest, der sich lohnt.

