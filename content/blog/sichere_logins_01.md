---
title: Sichere Logins - Teil 1 - Sichere und merkbare Passwörter
date: "2021-03-01T00:00:00+00:00"
tags:
- Anfänger
categories:
- Allgemeines
banner: "/img/thumbnail/sichere_logins_01.png"
series: sichere_logins
description: >
  Logins sind ein elementarer Bestandteil in der digitalen Welt von heute.
  Was leider auch ein Bestandteil davon ist, sind immer wieder Schlagzeilen
  über geknackte Konten und den Schabernack.
  Auch weil Endnutzerinnen und Endnutzer unsichere Logins nutzen. Wie Du das vermeiden kannst, wird Dir in diesem Beitrag verständlich erklärt.
---

## Sichere Logins - Teil 1 - Sichere und merkbare Passwörter

**Inhaltsverzeichnis:**

1. [Einleitung](#einleitung)
2. [Wodurch sich gute Passwörter auszeichnen](#wodurch-sich-gute-passw%C3%B6rter-auszeichnen)
3. [Die Kunst, sichere und merkbare Passwörter zu wählen](#die-kunst-sichere-und-merkbare-passw%C3%B6rter-zu-w%C3%A4hlen)
4. [Fazit](#fazit)
5. [Literatur und Quellen](#literatur-und-quellen)

### Einleitung

Die Bildschirmsperre beim PC und Laptop oder die Anmeldung beim E-Mail-Konto, beim Online-Banking oder beim Online-Shopping - heutzutage nutzt fast jede und jeder unzählige digitale Geräte und Dienste, bei denen man sich in irgendeiner Weise einloggen - authentifizieren - muss. Das ist auch gut so, denn nur so kann sichergestellt werden, dass Deine digitalen Dienste nicht von anderen in Deinem Namen missbraucht werden. Logins sind ein elementarer Bestandteil in der digitalen Welt von heute. Was leider auch ein Bestandteil davon ist, sind immer wieder Schlagzeilen über geknackte Konten und den Schabernack, den Kriminelle damit treiben. Warum? Weil entweder Diensteanbieterinnen und Diensteanbieter ihre Dienste schlecht absichern oder die Endnutzerinnen und Endnutzer unsichere Logins nutzen. Oft ist das keine Absicht, sondern schlicht Unwissen. Gegen ersteres kannst Du nur begrenzt etwas machen, gegen letzteres schon. Genau in diese Lücke zielt diese Artikelserie - Du wirst merken, es ist im Grunde kein Hexenwerk ausreichend sichere Logins zu nutzen.

### Die Rolle von Passwörtern

Eine der meistgenutzten Methoden um sich in digitale Geräte und Dienste einzuloggen, sind Passwörter. Es ist sehr wahrscheinlich, dass auch Du, sowie Du diesen Text ließt, jeden Tag die kleinen Ansammlungen von Buchstaben und Zahlen brauchst, um Dich in eben Deine digitalen Dienste und Gerätschaften einzuloggen. Womöglich sogar bei all den vorab genannten Beispielen wie bei Deinem PC oder Laptop, Smartphone oder Tablet, bei diversen Webseiten und so weiter. Inzwischen hat wohl jede und jeder mindestens ein paar digitale Dienste, die mit Passwort gesichert werden wollen. Passwörter spielen in der heutigen digitalen Welt eine entscheidende Rolle. Deswegen soll in diesem ersten Teil unserer Artikelserie erklärt werden, wodurch sich sichere Passwörter auszeichnen und wie man Passwörter wählt, die möglichst gleichzeitig sicher und dennoch merkbar sind.

### Wodurch sich sichere Passwörter auszeichnen

Die Sicherheit eines Passwortes misst sich daran, wie lange eine Angreiferin oder ein Angreifer braucht, es herauszufinden. Daraus ergeben sich zwei entscheidende Sicherheitsfaktoren für ein Passwort: Es muss schwer zu erraten sein und es muss geheim bleiben. Das bedeutet:

Sicher Passwörter sollten

1. **Zufällig sein.** Man sollte sich Passwörter nicht selber ausdenken, sondern zufällig generieren. Das mindert das Risiko, dass Parteien, die Deine Interessen, Deinen Charakter oder Deine Denkweise kennen, Dein Passwort erraten können.
2. **So lang wie möglich sein.** Nur ein zufälliges Passwort reicht nicht. Wenn das Passwort zu kurz ist, kann es leicht durch ausprobieren geknackt werden. Computer werden immer besser und können so immer mehr Passwörter in der gleichen Zeit ausprobieren - vor allem, wenn die genutzen Dienste dagegen nur unzureichend Vorkehrungen treffen. Mindestens 12 Zeichen für Onlinedienste und 20 Zeichen für WLAN-Zugänge sind die Empfehlung des Bundesamtes für Sicherheit in der Informationstechnik (BSI).
3. **So viele Zeichenarten wie möglich beinhalten.** Nicht nur die Länge des Passworts, auch die Anzahl der Zeichenarten erhöht die Anzahl möglicher Kombinationen des Passworts und schützt so gegen Erraten des Passworts durch ausprobieren. Dafür bieten sich gängige Zeichenarten wie Groß- und Kleinbuchstaben, Zahlen und Sonderzeichen an.
4. **Einzigartig sein.** Passwörter sollten nur für einen einzigen Dienst verwendet werden. Das Passwort für das Online-Banking sollte sich also vom Passwort für das E-Mail-Konto unterscheiden. So wird der Schaden minimiert, falls ein Zugang oder Passwort geknackt werden sollte.

Für sichere Passwörter gilt es **zu vermeiden**, dass diese

1. **Aufeinanderfolgende Zahlen, Buchstaben oder Tasten sind.** Solche Kombinationen sind leicht zu erraten, schnell ausprobiert und leicht abzuschauen, wenn man das Passwort auf der Tastatur eintippt.
2. **Persönliche Informationen enthalten.** Der Name, das Geburtsdatum oder das Lieblingsessen haben in Passwörtern nichts zu suchen. Solche Merkmale lassen sich leicht herausfinden und so das Passwort erraten.
3. **Unveränderte Standardpasswörter sind, die der Anbieter oder die Anbieterin Dir übermittelt haben.** Bei Standardpasswörtern kannst Du nicht überprüfen wer das Passwort noch kennt oder wo es unter Umständen noch gespeichert ist.
4. **Leicht zugänglich abgelegt werden:** Passwörter sollten nicht im Klartext im Downloads-Ordner liegen oder auf einem Zettel am PC kleben oder sonst an irgendwelchen Orten aufbewahrt werden, an denen es sich vermeiden lässt.

### Die Kunst, sichere und merkbare Passwörter zu wählen

Die oben genannten Merkmale sind recht anspruchsvoll und es stellt für die meisten Menschen eine Herausforderung dar, sich zufällig generierte, mehr als 12 Stellen lange Zeichenketten zu merken. Glücklicherweise gibt es Tricks, wie man die Aspekte für ein sicheres Passwort weitestgehend einhält und dieses dennoch gut zu merken ist.

#### Passphrasen

Passphrasen sind eine Reihe zufällig aneinandergereihter Wörter. Die Wörter entstammen einer Wortliste die z.B. auf Deutsch oder auf Englisch sein kann. Wenn man möchte, kann man die Worte noch durch Sonderzeichen der eigenen Wahl abtrennen. So entstehen im Endeffekt Passwörter, die man sich gut merken kann. Sicher sind sie dennoch, denn es lassen sich damit alle der oben genannten Aspekte erfüllen (**Zufälligkeit**, **Länge**, **Zeichenvielfalt**, **Einzigartigkeit**).

Das könnte beispielsweise dann wie folgt aussehen:

> anonym&robust+kraftwaltzfegtloesen

**Wie erzeugt man Passphrasen?**

Passphrasen lassen sich zum Beispiel durch die Diceware-Methode erzeugen. Man braucht dafür nur einen Würfel und eine [Wortliste](http://world.std.com/\~reinhold/diceware_german.txt):

1. Fünf mal mit dem Würfel würfeln und sich die Zahlen merken oder notieren, z.B. *13341*
2. Mithilfe der Zahlen das zugehörige Wort in der Wortliste suchen, in unserem Beispiel *anonym*
3. Die Schritte eins und zwei beliebig wiederholen, es empfiehlt sich eine Länge von mindestens fünf, besser sechs Wörtern. Siehe auch [Wikipedia: Sicherheitsberechnung](https://de.wikipedia.org/wiki/Diceware#Sicherheitsberechnung)
4. Sonderzeichen zwischen einigen oder allen Wörtern einfügen

#### Eselsbrücken

Eine weitere Methode ist es, sich einen Satz zu überlegen, den man sich merken kann und davon die Anfangsbuchstaben und Satzzeichen zu nutzen. Ein Beispiel dafür wäre "Anoxinon ist der beste Verein der Welt, denn er erklärt mir, wie ich sichere Passwörter erstellen kann!", was somit zu "AidbVdW,deem,wisPek!" wird: 

> AidbVdW,deem,wisPek!

Wichtig ist, dass auch dieser Satz möglichst frei erfunden und nicht aus irgendeinem Textstück stammt. Auch bei dieser Methode können alle oben genannten Aspekte erfüllt werden (mit gewisser Einschränkung bei der Zufälligkeit!) und so ein sicheres und merkbares Passwort entstehen.

**Hinweis:** Letztendlich bietet die Passphrase-Methode ein mathematisch ermittelbares Maß an Sicherheit, während das bei der Eselsbrücken-Methode nicht der Fall ist. Gut überlegt ist die Eselsbrücken-Methode dennoch deutlich besser, als keine Ordnung und Methodik bei der Passwortwahl und -organisation zu haben. 

### Fazit

Es ist im Grunde gar nicht so schwer, sichere Passwörter zu erstellen, die man sich doch einigermaßen merken kann. Es kann auch nur jeder und jedem geraten werden, die Vorschläge soweit wie möglich zu beachten, denn der Aufwand hält sich und Grenzen und die gewonnene Sicherheit ist es Wert - denn der Stress, der entstehen kann, wenn Passwörter ans Licht kommen, der ist es nicht und muss nicht sein.

Wenn man das alles beachtet, ist es natürlich klar, dass das für drei, vier, vielleicht fünf Passwörter gut funktioniert, aber dann die meisten von uns früher oder später Probleme haben werden, sich mehr Passwörter zu merken. Deswegen sollen im weiteren Verlauf dieser Artikelserie gezeigt werden, wie man dieses Problem mit sogenannten Passwortmanagern so gut wie möglich umgehen kann.

### Literatur und Quellen

* [Wikipedia: Passwort](https://de.wikipedia.org/wiki/Passwort)
* [Wikipedia: Passphrase](https://en.wikipedia.org/wiki/Passphrase)
* [Wikipedia: Diceware](https://de.wikipedia.org/wiki/Diceware)
* [BSI: Sichere Passwörter erstellen](https://www.bsi-fuer-buerger.de/BSIFB/DE/Empfehlungen/Passwoerter/passwoerter_node.html)
* [Verbraucherzentrale: Sichere Passwörter - so geht's](https://www.verbraucherzentrale.de/wissen/digitale-welt/datenschutz/sichere-passwoerter-so-gehts-11672)
