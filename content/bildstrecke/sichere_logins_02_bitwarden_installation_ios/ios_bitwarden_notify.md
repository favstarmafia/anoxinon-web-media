---
title: Bitwarden einrichten  
description: Die eben erwähnte automatische Synchronisierung läuft im Hintergrund mittels Benachrichtigungen ab. Erlaube diese also an der Stelle durch Druck auf "Erlauben". 
image: /img/sichere_logins_02/ios_bitwarden_firststart_04_e.png
weight: 11
---
