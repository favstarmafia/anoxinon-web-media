---
title: Bitwarden einrichten  
description: Wenn Du Bitwarden auf mehreren Geräten verwendest, kann es Deine Zugangsdaten automatisch synchron halten - Änderungen auf einem Gerät kommen automatisch auf dem anderen Gerät an. Bestätige daher die Dialogbox mit "Ok, verstanden!". 
image: /img/sichere_logins_02/ios_bitwarden_firststart_03_e.png
weight: 10
---
