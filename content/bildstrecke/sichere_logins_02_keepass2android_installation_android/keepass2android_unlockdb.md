---
title: KeePass2Android einrichten 
description: Nun kannst Du Deine Passwortdatei ensperren, dafür musst Du das eben gewählte Master-Passwort eingeben und "ENTSPERREN" anwählen. 
image: /img/sichere_logins_02/11_android_keepass2android_e.PNG
weight: 10
---
