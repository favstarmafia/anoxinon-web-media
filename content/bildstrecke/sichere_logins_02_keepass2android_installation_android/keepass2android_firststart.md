---
title: KeePass2Android einrichten 
description: Zunächst musst Du Dir eine Passwortdatei anlegen, in der alle Deine Zugänge von KeePass2Android verschlüsselt abgespeichert werden können. Wähle dafür "Neue Datenbank erstellen...". 
image: /img/sichere_logins_02/07_android_keepass2android_e.PNG
weight: 7
---
