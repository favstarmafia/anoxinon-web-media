---
title: KeePass2Android installieren
description: Nun dauert es kurz, KeePass2Android wird installiert. Den Fortschritt kannst Du über den Ladekreis um das KePass2Android-Symbol beobachten. 
image: /img/sichere_logins_02/05_android_keepass2android_e.png
weight: 5
---
