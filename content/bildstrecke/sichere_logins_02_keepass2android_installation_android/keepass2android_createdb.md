---
title: KeePass2Android einrichten 
description:  Für die Passwortdatei kannst Du bei (1) einen Speicherort und einen Dateinamen wählen. Wenn Dir das egal ist, kannst Du es einfach dabei belassen. Wichtig wird es bei (2), dort musst Du das Master-Passwort für Deinen Passwortmanager wählen. Dieses sollte unbedingt sicher sein, denn damit wird der Zugang zu KeePass2Android und damit allen Deinen Passwörtern geschützt! Genauso wenig solltest Du es vergessen, denn ohne gibt es keine Möglichkeit mehr, an Deine gespeicherten Zugänge zu kommen! 
image: /img/sichere_logins_02/08_android_keepass2android_e.PNG
weight: 8
---
