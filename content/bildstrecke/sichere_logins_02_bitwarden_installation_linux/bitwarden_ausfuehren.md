---
title: Bitwarden starten 
description: Abschließend kann Bitwarden über einen Doppelklick und anschließende Bestätigung gestartet werden. Bei Bedarf kann man sich noch einen Startmenü-Eintrag erstellen, das geht jedoch über diesen Artikel hinaus. Bei einem Update wiederholt man die eben gezeigten Schritte. 
image: /img/sichere_logins_02/bitwarden_ausführen_e.png
weight: 7
---
