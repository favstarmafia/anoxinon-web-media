---
title: KeePassXC für Edge installieren 
description: Nachfolgend kannst Du das Addon einfach mit Klick auf "Abrufen" installieren.
image: /img/sichere_logins_02/edge_keepassxc_05_e.png
weight: 5
---
