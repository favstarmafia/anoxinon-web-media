---
title: Strongbox installieren 
description: Nun musst Du noch die Installation mit Deinem Apple-ID Passwort, der Touch- oder Face-ID bestätigen. 
image: /img/sichere_logins_02/ios_appstore_strongbox_installconfirm_e.png
weight: 6
---
