---
title: Strongbox entsperren 
description: Da alle Zugänge bei Strongbox in einer simplen Datei auf Deinem iPhone gespeichert werden, ist es wichtig, dass Du diese regelmäßig sicherst. Denn wenn die Datei beschädigt wird oder verloren geht, kommst Du nicht mehr an Deine Zugänge im Passwortmanager. Vor allem ein Mobilgerät wie das iPhone verliert sich bekanntermaßen schnell. Daher hast Du hier die Möglichkeit eine Erinnerung für Backups zu setzen, zum Beispiel alle 2 Wochen.
image: /img/sichere_logins_02/ios_strongbox_firststart_20_e.PNG
weight: 30
---
