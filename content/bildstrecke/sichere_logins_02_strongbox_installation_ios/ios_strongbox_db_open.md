---
title: Strongbox ist bereit! 
description: Wenn alles geklappt hat, siehst Du nun an der Stelle Deine entsperrte Passwortdatei! 
image: /img/sichere_logins_02/ios_strongbox_firststart_22_e.PNG
weight: 32
---
