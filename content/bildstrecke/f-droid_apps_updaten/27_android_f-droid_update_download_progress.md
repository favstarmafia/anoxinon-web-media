---
title: Aktualisierungen durchführen
description: Falls die Updates noch nicht im Hintergrund heruntergeladen wurden, werden die Updates nun heruntergeladen.
image: /img/f-droid_als_app_bezugsquelle/27_android_f-droid_update_download_progress.png
weight: 27
---
