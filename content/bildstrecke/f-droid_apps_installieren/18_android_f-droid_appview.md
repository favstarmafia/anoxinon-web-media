---
title: App aufrufen
description: Wenn man eine App gefunden hat und installieren möchte, dann findet sich in der App-Übersicht eine "Installieren"-Schaltfläche, die genau das ermöglicht. Hier am Beispiel einer 7-Minute-Training-App.
image: /img/f-droid_als_app_bezugsquelle/18_android_f-droid_appview_e.png
weight: 18
---
