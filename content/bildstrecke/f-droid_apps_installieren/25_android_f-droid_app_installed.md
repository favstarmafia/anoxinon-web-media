---
title: App ist installiert
description: Fertig! Die App ist installiert und startbereit!
image: /img/f-droid_als_app_bezugsquelle/25_android_f-droid_app_installed.md
weight: 25
---
