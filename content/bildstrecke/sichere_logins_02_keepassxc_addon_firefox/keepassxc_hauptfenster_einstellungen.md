---
title: KeePassXC-Addon mit KeePassXC verbinden 
description: Das Addon muss noch verbunden werden. Das muss zuvor noch in KeePasXC erlaubt werden - wechsle dazu in Dein KeePassXC-Programm und klicke unter "Werkzeuge" auf "Einstellungen".
image: /img/sichere_logins_02/keepassxc_menue_einstellungen_e.png
weight: 8
---
