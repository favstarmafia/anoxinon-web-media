---
title: KeePassXC-Addon mit KeePassXC verbinden 
description: Nun kannst Du zurück im Firefox das kleine, neu hinzugekommene KeePassXC-Symbol in der oberen Leiste anwählen. Wenn Du Firefox nicht in der Zwischenzeit neu gestartet hast, musst Du dort auf "Neu laden" klicken, damit das Addon Dein KeePassXC-Programm erkennen kann. Beachte allerdings, dass Dein KeePassXC-Passwortspeicher dazu entsperrt sein muss - sonst passiert nichts!!! (Standardmäßig sperrt sich KeePassXC nach einer Zeit von selbst wieder.) 
image: /img/sichere_logins_02/keepassxc_firefox_startfenster_e.png
weight: 10
---
