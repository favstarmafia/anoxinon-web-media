---
title: KeePassXC-Addon mit KeePassXC verbinden 
description: Nun kannst Du zurück im Chrome das kleine, neu hinzugekommene Puzzleteil-Symbol in der oberen Leiste wählen. Dort findest Du das KeePassXC-Addon. Es empfiehlt sich, KeePassXC durch anwählen der kleinen Pinnadel an die Chrome-Leiste anzupinnen. 
image: /img/sichere_logins_02/chrome_keepassxc_10_e.png
weight: 10
---
