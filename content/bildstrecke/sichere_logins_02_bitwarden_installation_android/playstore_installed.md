---
title: Bitwarden ist installiert 
description: Nachdem Bitwarden installiert ist, kannst Du es öffnen um die Einrichtung abzuschließen. 
image: /img/sichere_logins_02/06_bitwarden_android_open.png
weight: 6
---
