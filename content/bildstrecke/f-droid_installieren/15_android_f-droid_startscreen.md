---
title: F-Droid ist installiert!
description: Fertig! F-Droid ist betriebsbereit eingerichtet!
image: /img/f-droid_als_app_bezugsquelle/15_android_f-droid_startscreen.png
weight: 15
---
