---
title: F-Droid ist installiert!
description: Nach dem initialen Start braucht F-Droid einige Sekunden, um die aktuelle App-Liste abzurufen.
image: /img/f-droid_als_app_bezugsquelle/14_android_f-droid_packagesourcesupdate.png
weight: 14
---
