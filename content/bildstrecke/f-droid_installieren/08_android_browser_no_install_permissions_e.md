---
title: F-Droid installieren
description: Um F-Droid zu installieren, benötigt dein Browser die Berechtigung zum Installieren von Applikationen. Folge der Schaltfläche zu den Einstellungen, um die Berechtigung zu vergeben und den Browser als Quelle zu erlauben.
image: /img/f-droid_als_app_bezugsquelle/08_android_browser_no_install_permissions_e.png
weight: 8
---
