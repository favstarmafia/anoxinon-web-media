---
title: F-Droid installieren
description: Dir wird angezeigt, dass dem Browser als Installations-Quelle nicht vertraut wird. Das musst du mit dem Schalter ändern.
image: /img/f-droid_als_app_bezugsquelle/09_android_browser_untrusted_source_e.png
weight: 9
---
