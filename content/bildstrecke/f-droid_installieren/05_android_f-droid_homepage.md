---
title: F-Droid herunterladen
description: Auf der F-Droid Webseite findet sich eine große "Herunterladen"-Schaltfläche. Unter dieser findest du den F-Droid-Download.
image: /img/f-droid_als_app_bezugsquelle/05_android_f-droid_homepage_e.png
weight: 5
---
