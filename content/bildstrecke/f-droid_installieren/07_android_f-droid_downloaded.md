---
title: F-Droid herunterladen
description: Nach Abschluss des Downloads gibt dir dein Browser bescheid. Das Aussehen variiert je nach Browser. Suche nach der Schaltfläche zum Öffnen der Installationsdatei und wähle diese an.
image: /img/f-droid_als_app_bezugsquelle/07_android_f-droid_downloaded_e.png
weight: 7
---
