---
title: F-Droid herunterladen
description: Alternativ kannst du die App-Übersicht mit allen Apps öffnen und deinen Browser von dort ansteuern.
image: /img/f-droid_als_app_bezugsquelle/02_android_app_picker_e.png
weight: 2
---
