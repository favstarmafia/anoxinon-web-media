---
title: KeePassXC einrichten 
description: Nun wird es wichtig. Du musst Dir ein Passwort für KeePassXC ausdenken. Das ist das Masterpasswort, das Deinen Passwortmanager und dessen Inhalt schützt. Es sollte daher wirklich sicher sein. Genauso wenig solltest Du es vergessen, ohne es kommst Du nicht mehr an Deine gespeicherten Zugänge. Wenn Du nicht mehr weißt, wie man sichere und merkbare Passwörter erstellt, schau' doch mal in Teil 1 dieser Artikelserie vorbei. Mit "Fertig" kannst Du die Einstellungen abschließen. 
image: /img/sichere_logins_02/12_macos_keepassxc_createdb_pw.png
weight: 12
---
