---
title: KeePassXC einrichten 
description: MacOS hakt nochmal nach, ob Du KeePassXC wirklich starten möchtest, das kannst Du bestätigen. 
image: /img/sichere_logins_02/07_macos_keepassxc_open.png
weight: 7
---
