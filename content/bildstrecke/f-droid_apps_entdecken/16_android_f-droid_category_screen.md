---
title: Kategorien durchsehen
description: Genauso kann man den Tab "Kategorien" verwenden. Dort sind die F-Droid-Apps nach Kategorie aufgegliedert und durchsuchbar. Natürlich kann man auch direkt nach Apps suchen, das geht mit der kleinen, grünen Lupen-Schaltfläche.
image: /img/f-droid_als_app_bezugsquelle/16_android_f-droid_category_screen_e.png
weight: 16
---
