---
title: Neue Apps entecken
description: Es gibt veschiedene Wege, Apps in F-Droid zu entdecken. Bspw. im Tab "Neues". Dort sind neu hinzugefügte oder kürzlich aktualisierte Applikationen sichtbar.
image: /img/f-droid_als_app_bezugsquelle/15_android_f-droid_startscreen_e.png
weight: 15
---
