---
title: KeePassXC erster Start
description: Nun kannst Du loslegen - Um Passwörter zu speichern, musst Du eine Passwortdatei erstellen, bei KeePassXC "Datenbank" genannt. Dazu auf "Neue Datenbank erstellen" klicken.
image: /img/sichere_logins_02/14_win_keepassxc_launched_e.png
weight: 14
---
