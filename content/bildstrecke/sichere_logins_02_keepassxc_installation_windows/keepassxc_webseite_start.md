---
title: KeePassXC Startseite 
description: Unter keepassxc.org hat man zentral einen gelben Knopf "Download for Windows" um zu den Downloads zu gelangen.
image: /img/sichere_logins_02/01_win_keepassxc_webseite_e.png
weight: 1
---
