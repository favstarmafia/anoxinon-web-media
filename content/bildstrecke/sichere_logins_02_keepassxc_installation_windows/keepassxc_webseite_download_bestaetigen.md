---
title: KeePassXC herunterladen 
description: Nun kann man KeePassXC mit Klick auf "Datei speichern" herunterladen.
image: /img/sichere_logins_02/03_win_keepassxc_safe_file_firefox_e.png
weight: 3
---
