---
title: Bitwarden-Addon anmelden
description: Ab sofort findest Du oben rechts in der Firefox-Leiste das Symbol für Bitwarden. Du musst Dich noch anmelden, dafür einfach auf den gleichnamigen Knopf klicken.
image: /img/sichere_logins_02/bitwarden_firefox_startfenster_e.png
weight: 6
---
