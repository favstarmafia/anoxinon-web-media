---
title: Bitwarden für Firefox installieren 
description: Nachfolgend kannst Du das Addon einfach mit Klick auf "Hinzufügen" installieren.
image: /img/sichere_logins_02/mozilla_addons_bitwarden_e.png
weight: 4
---
