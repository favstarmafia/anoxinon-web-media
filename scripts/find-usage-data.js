const { readFileSync } = require('fs')
const { execSync } = require('child_process')
const glob = require('glob')
const cheerio = require('cheerio')

function add(list, item) {
  if (list.indexOf(item) === -1) list.push(item)
}

module.exports = function() {
  console.log('run hugo')
  execSync('hugo')

  console.log('search html files')
  const htmlFiles = glob.sync('public/**/*.html')

  console.log('find used tags, classes and ids')
  const result = { tags: [], ids: [], classes: [] }
  htmlFiles.forEach((page) => {
    const $ = cheerio.load(readFileSync(page))
    
    $('*').each((_, el) => {
      const $el = $(el)
      
      const classes = ($el.attr('class') || '').split(' ').filter((item) => item.length > 0)
      const id = $el.attr('id') || null
      const tag = el.tagName
      
      classes.forEach((className) => add(result.classes, className))
      if (id) add(result.ids, id)
      add(result.tags, tag)
    })
  })
  
  return result
}
