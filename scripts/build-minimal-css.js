const { readFileSync, writeFileSync } = require('fs')
const { resolve } = require('path')
const csso = require('csso')
const findUsedIcons = require('./find-used-icons.js')
const findUsageData = require('./find-usage-data.js')

const usedIcons = findUsedIcons()
const usageData = findUsageData()

console.log('load input css files')
const customCssSource = readFileSync(resolve(__dirname, '../themes/anoxinonmedia/static/css/style.css')).toString()
const fontCssSource = readFileSync(resolve(__dirname, '../themes/anoxinonmedia/static/css/fork-awesome.min.css')).toString()

console.log('minify css')
const minimalCss = customCssSource + fontCssSource
const minifiedCss = csso.minify(minimalCss, { usage: usageData }).css

console.log('save css')
writeFileSync(resolve(__dirname, '../themes/anoxinonmedia/static/css/style.min.css'), minifiedCss)
